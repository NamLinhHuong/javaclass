const webpack = require('webpack');

module.exports = {
  entry: './src/js/index.js',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      }
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  output: {
//    path: __dirname + '/src/main/resources/static/dist', // production spring boot
    path: __dirname + '/target/classes/static/dist', // local spring boot
//    publicPath: '/',
//    publicPath: '', // npm
    publicPath: 'resources/dist/', // spring boot + production spring boot
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  devServer: {
    contentBase: './src/main/resources/static/dist',
    hot: true
  }
};
