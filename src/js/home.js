import React, { Component } from 'react';
import Chat from './app-chat/home';
import GameOne from './game-one/home';
import App3D from './app-3D/home';
import AppOne from './app-one/home';
//import {
//  Container, Row, Col, Button, ListGroup, ListGroupItem } from 'reactstrap';
import {redirect, removeAuthCookie, enableInfinity, disableInfinity, existInfinity} from './utils'
//import { Route, Redirect } from 'react-router'
const axios = require('axios');

export default class Home extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isOpen: false,
            profile: null,
            showSidebar: false,
            redirect: null,
            isHeader: true,
            isMenu: false,
            isFooter: true,
            isHome: true,
            infinity: false,
            appName: null
        };
        this.loadProfile = this.loadProfile.bind(this);
        this.updateProfile = this.updateProfile.bind(this);
        this.clickLogout = this.clickLogout.bind(this);
        this.handleSuccess = this.handleSuccess.bind(this);
        this.handleFailure = this.handleFailure.bind(this);
        this.redirect = this.redirect.bind(this);
        this.renderMainMenu = this.renderMainMenu.bind(this);
        this.toggleHeader = this.toggleHeader.bind(this);
        this.toggleFooter = this.toggleFooter.bind(this);
        this.toggleMenu = this.toggleMenu.bind(this);
        this.toggleHome = this.toggleHome.bind(this);
        this.toggleSizeBar = this.toggleSizeBar.bind(this);
        this.startApp = this.startApp.bind(this);
        this.renderApp = this.renderApp.bind(this);
        this.closeApp = this.closeApp.bind(this);
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    componentDidMount() {
        this._isMounted = true;
        window.addEventListener("resize", this.resize.bind(this));
        this.resize();

        var saveData = JSON.parse(localStorage.saveData || null) || {};
        console.log(saveData)
        if (saveData.token) {
            this.loadProfile(saveData.token);
        }
    }

    loadProfile(token) {
        axios.get('/api/profile',{headers:{"Authorization":"Bearer: "+token}})
            .then(this.handleSuccess)
            .catch(this.handleFailure);
    }

    handleSuccess(response) {
        if (this._isMounted) this.updateProfile(response.data);
    }

    handleFailure(response) {
        if (this._isMounted) console.log('BACKEND: '+JSON.stringify(response.data))
    }

    updateProfile(profile) {
        this.setState({profile: profile});
    }

    clickLogout() {
        localStorage.saveData = null;
        removeAuthCookie();
        window.location.href = '/logout';
    }

    resize() {
        if (this._isMounted) this.setState({smallScreen: window.innerWidth <= 600});
    }

    redirect(url) {
        this.setState({redirect: url})
    }

    renderMainMenu() {
        const mainMenu = (
            <div>
                <img width="40" height="40" style={mainMenuButton} src={"/resources/icon.png"}
                    onClick={() => {this.setState({showSidebar: !this.state.showSidebar})}}/>
                {this.state.showSidebar &&
                    <div>
                        <button style={mainMenuButton2} onClick={() => {this.redirect("/")}} id="mainMenuButton2" title="Home">Home</button>
                        <button style={{...mainMenuButton2, bottom:'120px'}} onClick={() => window.location.replace("logout")} id="mainMenuButton2" title="Log Out">Log out</button>
                        <button style={{...mainMenuButton2, bottom:'180px'}} onClick={() => {this.toggleInfinity();this.toggleSizeBar();}} id="mainMenuButton2" title="Infinity">Infinity</button>
                    </div>
                }
            </div>
        )
        return mainMenu;
    }

    renderFooter() {
        return (
            <footer style={footerStyle} className="my-5 pt-5 text-muted text-center text-small">
                <p className="mb-1">&copy; 2018-2019 breakaloop.com</p>
            </footer>
        )
    }

    toggleHome() {
        let isHome = !this.state.isHome;
        let show = false;
        if (isHome) {
            show = true;
        }
        this.setState({isHome: isHome, isFooter: show, isHeader: show, isMenu: false})
    }

    toggleFooter() {
        this.setState({isFooter: !this.state.isFooter})
    }

    toggleHeader() {
        this.setState({isHeader: !this.state.isHeader})
    }

    toggleMenu() {
        this.setState({isMenu: !this.state.isMenu})
    }

    toggleSizeBar() {
        this.setState({showSidebar: !this.state.showSidebar})
    }

    startApp(appName) {
        this.setState({appName: appName})
    }

    closeApp() {
        this.setState({appName: null})
    }

    renderApp() {
        let app = null;
        if (this.state.appName === 'chat') {
            app = (<Chat smallScreen={this.state.smallScreen} closeApp={this.closeApp}/>);
        } else if (this.state.appName === 'app-test-sanball7') {
            app = (<SanBall7 smallScreen={this.state.smallScreen} closeApp={this.closeApp}/>);
        } else if (this.state.appName === 'app-one') {
            app = (<AppOne smallScreen={this.state.smallScreen} closeApp={this.closeApp} toggleHome={this.toggleHome}/>);
        } else if (this.state.appName === 'game-one') {
            app = (<GameOne smallScreen={this.state.smallScreen} closeApp={this.closeApp}/>);
        }
        return app;
    }

    toggleInfinity() {
        let infinity = !this.state.infinity;
        this.setState({infinity: infinity})
        if (infinity) {
            if (existInfinity()) {
                enableInfinity();
            } else {
                App3D(this.oneRootElement);
            }
        } else {
            disableInfinity();
        }
    }

    render() {
//        let redirectUrl = this.state.redirect;
//        if (redirectUrl && location.pathname != redirectUrl) {
//            return (<Redirect to={redirectUrl}/>);
//        }

        if (this.state.smallScreen) {
            console.log('smallllll')
        } else {
        }

        var userImageUrl = null;
        var userName = null;
        const profile = this.state.profile;
        if (profile) {
            userImageUrl = profile.imageUrl;
            userName = profile.displayName;
        }

        const fixedHeader = (
            <div>
                <div className="navbar" style={fixedHeaderStyleNavBar}>
                    <a style={fixedHeaderStyleNavBarA} onClick={this.toggleMenu}>❯❯</a>
                    {!this.state.smallScreen &&
                        <a style={fixedHeaderStyleNavBarA} href="">breakaloop.com</a>
                    }
                    <div>
                        <a style={{...fixedHeaderStyleNavBarA, float:'right'}}>❮❮</a>
                        <img className="d-block mx-auto mb-4"
                            src={userImageUrl} alt="" width="34" height="34"
                            style={{float:'right',borderRadius:'50%',margin:'3px',padding:'2px',border:'1px solid #ddd'}}/>
                        <a style={{...fixedHeaderStyleNavBarA, float:'right'}} href="#contact">{userName}</a>
                    </div>
                </div>
            </div>
        )

        const fixedMenu = (
            <div className="l-nav" style={fixedMenuStyleLNav}>
                <nav className="nav" style={fixedMenuStyleNav}>
                    <ul style={fixedMenuStyleNavUL}>
                        <li style={fixedMenuStyleNavLI} className="nav-primary"><a style={fixedMenuStyleNavA} onClick={this.toggleMenu} data-scroll>❮❮</a></li>
                        <li style={fixedMenuStyleNavLI} className="nav-primary"><a style={fixedMenuStyleNavA} href="" data-scroll>Home</a></li>
                        <li style={fixedMenuStyleNavLI} className="nav-primary"><a style={fixedMenuStyleNavA} href="" data-scroll>Approach</a></li>
                        <li style={fixedMenuStyleNavLI} className="nav-primary"><a style={fixedMenuStyleNavA} href="" data-scroll>Work</a></li>
                        <li style={fixedMenuStyleNavLI} className="nav-primary"><a style={fixedMenuStyleNavA} href="" data-scroll>People</a></li>
                        <li style={fixedMenuStyleNavLI} className="nav-secondary"><a style={fixedMenuStyleNavA} href="">Jobs</a></li>
                        <li style={fixedMenuStyleNavLI} className="nav-secondary"><a style={fixedMenuStyleNavA} href="">Blog</a></li>
                        <li style={fixedMenuStyleNavLI} className="nav-secondary"><a style={fixedMenuStyleNavA} href="">Contact</a></li>
                    </ul>
                </nav>
            </div>
        )

        let mainStyle = null;
        let mainHeader = null;
        let mainMenu = null;
        if (this.state.isHeader) {
            mainStyle = {marginTop:'46px'}; ///// 46 = header
            mainHeader = fixedHeader;
        }
        if (this.state.isMenu) {
            mainStyle = {marginTop:'46px'}; ///// 46 = header
            //mainStyle = {marginLeft:'240px'};
            mainMenu = fixedMenu;
//            mainHeader = (
//                <div style={{marginLeft: '240px'}}>
//                {mainHeader}
//                </div>
//            );
        }

        let gridStyle = mc6;
        const blankStyle = {margin: '10px', padding: '10px', fontSize: '20px',background: '#bfbfbf', color: '#bfbfbf'}
        let blank = (
            <div>
                <div style={gridStyle}>
                    <div style={blankStyle}>{'.'}</div>
                </div>
                <div style={gridStyle}>
                    <div style={blankStyle}>{'.'}</div>
                </div>
            </div>
        );
        if (this.state.smallScreen) {
            gridStyle = mc12;
            blank = (
                <div>
                    <div style={gridStyle}>
                        <div style={blankStyle}>{'.'}</div>
                    </div>
                </div>
            );
        }
        let mainBody = (
            <div>
                <div style={gridStyle}>
                    <div style={mc} onClick={() => this.startApp('app-one')}>App One</div>
                </div>
                <div style={gridStyle}>
                    <div style={mc} onClick={() => this.startApp('chat')}>Chat</div>
                </div>
                <div style={gridStyle}>
                    <div style={mc} onClick={() => this.startApp('game-one')}>Game One</div>
                </div>
                <div style={gridStyle}>
                    <div style={mc} onClick={() => this.redirect("/web/werewolf")}>Werewolf</div>
                </div>
                <div style={gridStyle}>
                    <div style={mc} onClick={() => this.redirect("/web/my-native-canvas")}>Native Canvas</div>
                </div>
                <div style={gridStyle}>
                    <div style={mc} onClick={() => this.redirect("/web/my-canvas")}>Canvas</div>
                    </div>
                <div style={gridStyle}>
                    <div style={mc} onClick={() => this.redirect("/web/mon-man")}>Money Manager</div>
                    </div>
                <div style={gridStyle}>
                    <div style={mc} onClick={() => window.location.replace("swagger-ui.html")}>API</div>
                    </div>
                <div style={gridStyle}>
                    <div style={mc} onClick={this.toggleHeader}>Header</div>
                </div>
                <div style={gridStyle}>
                    <div style={mc} onClick={this.toggleMenu}>Menu</div>
                </div>
                {blank}
            </div>
        )

        if (this.state.appName) {
            mainBody = this.renderApp();
        }

        if (this.state.infinity) {
            return (
                <div>
                    {this.renderMainMenu()}
                    <div ref={element => this.oneRootElement = element} />
                </div>
            );
        } else {
            if (this.state.isHome) {
                document.body.style.background = '#bfbfbf';
                document.body.style.margin = '0px';
            } else {
                document.body.style.background = null;
                document.body.style.margin = null;
            }
            let footer = null;
            if (this.state.isFooter) {
                footer = this.renderFooter();
            }
            return (
                <div>
                    <br/>
                    {mainHeader}
                    {mainMenu}
                    <div className="container" style={mainStyle}>
                        {mainBody}
                        {this.renderMainMenu()}
                    </div>
                    {footer}
                </div>
            );
        }
    }
}

const mc   = {
    borderRadius: '10px',
    margin: '10px',
    padding: '10px',
    color: '#ffffff',
    fontSize: '20px',
    backgroundColor: '#808080',//'#00cc00',//'#33b5e5', // #0099cc
    boxShadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
    "&:hover": {
        backgroundColor: "#0099cc"
    }
}
const mc1  = {float: 'left', width: '8.33%'}
const mc2  = {float: 'left', width: '16.66%'}
const mc3  = {float: 'left', width: '25%'}
const mc4  = {float: 'left', width: '33.33%'}
const mc5  = {float: 'left', width: '41.66%'}
const mc6  = {float: 'left', width: '50%'}
const mc7  = {float: 'left', width: '58.33%'}
const mc8  = {float: 'left', width: '66.66%'}
const mc9  = {float: 'left', width: '75%'}
const mc10 = {float: 'left', width: '83.33%'}
const mc11 = {float: 'left', width: '91.66%'}
const mc12 = {float: 'left', width: '100%'}

const mainMenuButton = {
    opacity: 0.7,
    fontSize: '20px',
    position: "fixed",
    bottom: '5px',
    left: '10px',
    zIndex: '99', // Make sure it does not overlap
    border: 'none', // Remove borders
    outline: 'none', // Remove outline
    backgroundColor: 'white',
    background: 'rgba(255, 255, 255, 0)',
    color: 'white',
    cursor: 'pointer', // Add a mouse pointer on hover
    //padding: '15px',
    //fontSize: '18px' // Increase font size
    borderRadius: '10px' // Rounded corners
};

const mainMenuButton2 = {
    padding: '10px',
    borderRadius: '15px',
    opacity: 0.5,
    fontSize: '20px',
    position: "fixed",
    bottom: '60px',
    left: '5px',
    zIndex: '99', // Make sure it does not overlap
    border: 'none', // Remove borders
    outline: 'none', // Remove outline
    backgroundColor: 'black',
    color: 'white',
    cursor: 'pointer' // Add a mouse pointer on hover
};

/*
<style>
#mainMenuButton {
}
#mainMenuButton:hover {
    background-color: #555; // Add a dark-grey background on hover
}
</style>
*/




/* The navigation bar */
const fixedHeaderStyleNavBar = {
    height: '46px',
    overflow: 'hidden',
    backgroundColor: '#333',
    position: 'fixed', /* Set the navbar to fixed position */
    top: 0, /* Position the navbar at the top of the page */
    width: '100%', /* Full width */

    boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
    zIndex: 10, // :)))))))))))))))))))
};

/* Links inside the navbar */
const fixedHeaderStyleNavBarA = {//.navbar a {
  float: 'left',
  display: 'block',
  color: '#f2f2f2',
  textAlign: 'center',
  padding: '14px 16px',
  textDecoration: 'none',
};

/* Change background on mouse-over */
const fixedHeaderStyleNavBarHover = {//.navbar a:hover {
  background: '#ddd',
  color: 'black'
};

/* Main content */
const fixedHeaderStyleMain = {//main {
  marginTop: '30px' /* Add a top margin to avoid content overlay */
};


const fixedMenuStyleLNav = {
  position: 'absolute',
  width: '240px',
  display: 'block',
  background: '#3a4043',
  top: 0,
  bottom: 0,
  left: 0,
  zIndex: 11,
}
const fixedMenuStyleNav = {
    boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
  background: '#3a4043',


  width: '180px',
  position: 'fixed',
  top: 0,
  bottom: 0,
  margin: 0,
  padding: '30px',
  overflow: 'auto',
}
const fixedMenuStyleNavUL = {
  margin: 0,
  padding: 0,
  listStyle: 'none',
}
const fixedMenuStyleNavLI = {
  margin: 0,
  padding: 0,
//  -webkit-transition: 0.25s;
//  -moz-transition: 0.25s;
  transition: '0.25s',
}
const fixedMenuStyleNavA = {
  color: '#fff',
  textDecoration: 'none',
  fontSize: '20px',
  fontWeight: 800,
  display: 'block',
  padding: '10px 0',
}


const footerStyle = {
    textAlign: 'center',
    position: 'fixed',
    width: '100%',
    bottom: 0,
}