import React, { Component } from 'react';
//import React, { lazy, Suspense } from 'react';
import { Suspense } from 'react';
//import LazyChat from './lazy';
const LazyChat = React.lazy(() => import('./lazy'));

class Chat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            load: false,
        }
        this.load = this.load.bind(this);
    }

    load(event) {
        console.log('test '+this.state.load);
        this.setState({load: !this.state.load});
    }

    render() {
        let lazy = null;
        if (this.state.load) {
            lazy = (
                <div>
                  <Suspense fallback={<div>Loading...</div>}>
                    <LazyChat />
                  </Suspense>
                </div>
            );
        }
        return (
            <div>
                <div onClick={() => this.load()}>Test {JSON.stringify(this.state.load)}</div>
            OKKKKK
            {lazy}
            END
            </div>
        )
    }
}

export default Chat;
