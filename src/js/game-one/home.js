import React, { Component } from 'react';

class GameOne extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ok: null,
        }
        this.loadObject = this.loadObject.bind(this);
    }

    loadObject() {
        // http://localhost:8080/src/main/resources/static/built/myPrint.bundle.js
        import(/* webpackChunkName: "../../../../../resources/built/myPrint" */ './app').then(module => {
            var print = module.default;
            var ok = print();
            this.setState({ok: ok});
        })
    }

    render() {
        if (this.state.ok) {
            return this.state.ok;
        }

        let gridStyle = mc12;
        return (
            <div>
                <div style={gridStyle}>
                    <div style={mc} onClick={() => this.loadObject()}>Load</div>
                </div>
                <div style={gridStyle}>
                    <div style={mc} onClick={() => this.props.closeApp()}>Exit</div>
                </div>
            </div>
        )
    }
}

const mc = {
    borderRadius: '10px',
    margin: '10px',
    padding: '10px',
    color: '#ffffff',
    fontSize: '20px',
    backgroundColor: '#808080',
    boxShadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
}
const mc12 = {float: 'left', width: '100%'}

export default GameOne;
