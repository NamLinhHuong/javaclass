import * as THREE from 'three'
import {enableInfinity} from '../utils'

export default container => {
    var camera, scene, renderer;
    var geometry, material, mesh;

    init();
    animate();
    enableInfinity();

    function init() {

        camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 0.01, 10 );
        camera.position.z = 1;

        scene = new THREE.Scene();

        geometry = new THREE.BoxGeometry( 0.2, 0.2, 0.2 );

        //material = new THREE.MeshNormalMaterial();
        // Make a material
        material = new THREE.MeshPhongMaterial({
            //ambient: 0x555555,
            color: 0x555555,
            specular: 0xffffff,
            shininess: 50,
            //shading: THREE.SmoothShading
            flatShading: THREE.SmoothShading
        });

        mesh = new THREE.Mesh( geometry, material );
        scene.add( mesh );

        // Add 2 lights.
        let light1 = new THREE.PointLight(0xff0040, 2, 0);
        light1.position.set(200, 100, 300);
        scene.add(light1);
        //
        let light2 = new THREE.PointLight(0x0040ff, 2, 0);
        light2.position.set(-200, 100, 300);
        scene.add(light2);

        renderer = new THREE.WebGLRenderer( { antialias: true } );
        renderer.setSize( window.innerWidth, window.innerHeight );
        document.body.appendChild( renderer.domElement );
        renderer.domElement.id = "infinity-canvas";

        window.addEventListener( 'resize', onWindowResize, false );
        onWindowResize();

//        renderer.domElement.addEventListener("click", onclick, true);
        mesh.callback = objectClickHandler;
        //document.addEventListener('mousedown', onDocumentMouseDown, false);
        //mousemove
        document.addEventListener('mousedown', onclick, false);
    }

    function objectClickHandler(event) {
        console.log("ijijjijijiji" + Math.random() + " - " + event);
//            window.open('http://www.pericror.com/', '_blank');
    }

    var raycaster = new THREE.Raycaster();
    var selectedObject;
    var mouse = new THREE.Vector2();
    function onclick(event) {
        var mouse = new THREE.Vector2();
        raycaster.setFromCamera(mouse, camera);
        let meshObjects = [mesh];
//        var intersects = raycaster.intersectObjects(meshObjects, true); //array
//        if (intersects.length > 0) {
//            selectedObject = intersects[0];
//            alert(JSON.stringify(selectedObject));
//        }

        mouse.x = (event.clientX / renderer.domElement.clientWidth) * 2 - 1;
        mouse.y =  - (event.clientY / renderer.domElement.clientHeight) * 2 + 1;

        raycaster.setFromCamera(mouse, camera);

        var intersects = raycaster.intersectObjects(meshObjects);

        if (intersects.length > 0) {
            intersects[0].object.callback();
        }
    }

    function onWindowResize() {
        let w = window.innerWidth;
        let h = window.innerHeight;
        let aspectRatio = window.innerWidth / window.innerHeight;
        camera.aspect = aspectRatio;
//        camera.left = w / - 2 * viewSize;
//        camera.right = w / 2 * viewSize;
//        camera.top = h / 2 * viewSize;
//        camera.bottom = h / - 2 * viewSize;
        camera.updateProjectionMatrix();
        renderer.setSize( w, h );

        if (raycaster) {
            raycaster.setFromCamera(mouse, camera);
        }
    }

    function animate() {

        requestAnimationFrame( animate );

        mesh.rotation.x += 0.01;
        mesh.rotation.y += 0.02;

        renderer.render( scene, camera );

    }
}