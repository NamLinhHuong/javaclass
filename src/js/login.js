import React, { Component } from 'react';
//import { Frame } from 'react-keyframes';

class Login extends Component {
    constructor (props) {
        super(props);

        this.state = { cSelected: [], rotateIcon: 0 };

        this.createFrames = this.createFrames.bind(this);
        this.rotate = this.rotate.bind(this);
    }

    componentDidMount() {
      window.addEventListener("resize", this.resize.bind(this));
      this.resize();
      this.rotate();
    }

    resize() {
        console.log(this.state.smallScreen);
        this.setState({smallScreen: window.innerWidth <= 760});
    }

    createFrames() {
        var frames = [];

        var myArray = [20,40,60,80,100,120,140,160,180,200,220,240,260,280,300,320,340];
        myArray.forEach(function (value, i) {
            //console.log('%d: %s', i, value);
            /*frames.push(
                <Frame duration={50} key={i}>
                    <div className="d-flex justify-content-center">
                        <img width="180" height="180" style={{transform: 'rotate('+value+'deg)'}} src={"/resources/my-smile.png"}/>
                    </div>
                </Frame>
            );*/
            frames.push(
                <div duration={50} key={i}>
                    <div className="d-flex justify-content-center">
                        <img width="180" height="180" style={{transform: 'rotate('+value+'deg)'}} src={"/resources/my-smile.png"}/>
                    </div>
                </div>
            );
        });

        return frames;
    }

    rotate() {
        const milliseconds = 100;
        this.state.rotateIcon += 5;
        if (this.state.rotateIcon == 360) {
            this.state.rotateIcon = 0;
        }
        this.setState(this.state);
        setTimeout(this.rotate, milliseconds);
    }

    render() {
        const submitForm = (
            <div>
                <form id="submit-form" action="./signin/facebook" method="POST"
                    onSubmit={() => localStorage.saveData = null}>
                    <button className="btn-block" color="primary">login with facebook</button>
                </form>
                <br/>
                <form id="submit-form" action="./signin/google" method="POST"
                    onSubmit={() => localStorage.saveData = null}>
                    <button className="btn-block" color="primary">login with google</button>
                </form>
            </div>
        )
        if (this.state.smallScreen) {
            return (
                <div>
                    <div>
                        <br/>
                        <div className="d-flex justify-content-center">
                            <img width="100" height="100" style={{transform: 'rotate('+this.state.rotateIcon+'deg)'}} src={"/resources/icon.png"}/>
                        </div>
                        <br/>
                        <div className="d-flex justify-content-center">
                            <h3>breakaloop.com</h3>
                        </div>
                        <div className="d-flex justify-content-center">please login</div>
                        <br/>
                        <div className="d-flex justify-content-center">
                            {submitForm}
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div>
                    <div>
                        <br/>
                        <br/>
                        <br/>
                        <div className="d-flex justify-content-center">
                            <img width="180" height="180" style={{transform: 'rotate('+this.state.rotateIcon+'deg)'}} src={"/resources/icon.png"}/>
                        </div>
                        <br/>
                        <div className="d-flex justify-content-center">
                            <h1 className="display-3">breakaloop.com</h1>
                        </div>
                        <div className="d-flex justify-content-center">
                            <p className="lead" >please login</p>
                        </div>
                        <br/>
                        <div className="d-flex justify-content-center">
                            {submitForm}
                        </div>
                    </div>
                </div>
            )
        }
    }
};

export default Login;
