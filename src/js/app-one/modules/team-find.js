import React, { Component } from 'react';
import { Segment, Form } from 'semantic-ui-react'
import { getQueryString, updateQueryString, axiosGet, axiosPost, copyToClipboard } from '../../utils'
import { MyContext } from '../lazy'

export default class TeamFind extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.find = this.find.bind(this);
    }

    handleChange(event, { name, value }, storage) {
        storage.teamFind = {...storage.teamFind, [name]: event.target.value};
        storage.updateStorage(storage);
    }

    find(storage) {
        axiosGet('/api/san-ball-7/team?teamId='
                    + storage.teamFind.teamId
                    + '&name=' + storage.teamFind.name, null,
            function(response){
                storage.teamFind.response = response.data;
                storage.updateStorage(storage);
            }, function(response) {
                console.log(response);
            });
    }

    render() {
        return (
            <MyContext.Consumer>
                {({storage}) => (
                    <div>
                        <Form>
                            <Form.Group>
                                <Form.Field width={8}>
                                <Form.Input label='Team ID' name='teamId'
                                    value={storage.teamFind.teamId}
                                    onChange={(event, data) =>
                                        this.handleChange(event, data, storage)} />
                                </Form.Field>
                                <Form.Field width={8}>
                                <Form.Input label='Team Name' name='name'
                                    value={storage.teamFind.name}
                                    onChange={(event, data) =>
                                        this.handleChange(event, data, storage)} />
                                </Form.Field>
                            </Form.Group>
                            <Form.Group>
                                <Form.Field>
                                <Form.Button content='Find Team'
                                    onClick={() => this.find(storage)} />
                                </Form.Field>
                                <Form.Field>
                                <Form.Button content='Copy'
                                    onClick={() => copyToClipboard(JSON.stringify(storage.teamFind.response))} />
                                </Form.Field>
                            </Form.Group>
                        </Form>
                        <strong>response:</strong>
                        <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                            <pre> {JSON.stringify(storage.teamFind.response, null, 2)} </pre>
                        </Segment>
                    </div>
                )}
            </MyContext.Consumer>
        )
    }
}
