import React, { Component } from 'react';
import { Segment, Form } from 'semantic-ui-react'
import { getQueryString, updateQueryString, axiosGet, axiosPost, copyToClipboard } from '../../utils'
import { MyContext } from '../lazy'

export default class PitchList extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.list = this.list.bind(this);
    }

    handleChange(event, { name, value }, storage) {
        storage.pitchList = {...storage.pitchList, [name]: event.target.value};
        storage.updateStorage(storage);
    }

    list(storage) {
        axiosGet('/api/san-ball-7/pitch/all?page='
                    + storage.pitchList.page
                    + '&size=' + storage.pitchList.size, null,
            function(response){
                storage.pitchList.response = response.data;
                storage.updateStorage(storage);
            }, function(response) {
                console.log(response);
            });
    }

    render() {
        return (
            <MyContext.Consumer>
                {({storage}) => (
                    <div>
                        <Form>
                            <Form.Group>
                                <Form.Field width={8}>
                                <Form.Input label='page' name='page'
                                    value={storage.pitchList.page}
                                    onChange={(event, data) =>
                                        this.handleChange(event, data, storage)} />
                                </Form.Field>
                                <Form.Field width={8}>
                                <Form.Input label='size' name='size'
                                    value={storage.pitchList.size}
                                    onChange={(event, data) =>
                                        this.handleChange(event, data, storage)} />
                                </Form.Field>
                            </Form.Group>
                            <Form.Group>
                                <Form.Field>
                                <Form.Button content='Get Pitch List'
                                    onClick={() => this.list(storage)} />
                                </Form.Field>
                                <Form.Field>
                                <Form.Button content='Copy'
                                    onClick={() => copyToClipboard(JSON.stringify(storage.pitchList.response))} />
                                </Form.Field>
                            </Form.Group>
                        </Form>
                        <strong>response:</strong>
                        <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                            <pre> {JSON.stringify(storage.pitchList.response, null, 2)} </pre>
                        </Segment>
                    </div>
                )}
            </MyContext.Consumer>
        )
    }
}
