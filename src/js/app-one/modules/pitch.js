import React, { Component } from 'react';
import { Segment, Form } from 'semantic-ui-react'
import { getQueryString, updateQueryString, axiosGet, copyToClipboard } from '../../utils'
import { MyContext } from '../lazy'
import PitchList from './pitch-list'
import PitchAllService from './pitch-all-service'

export default class Pitch extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <MyContext.Consumer>
                {({storage, updateStorage}) => (
                    <Segment basic>
                        <Segment.Group>
                            <Segment><PitchList/></Segment>
                            <Segment><PitchAllService/></Segment>
                        </Segment.Group>
                    </Segment>
                )}
            </MyContext.Consumer>
        )
    }
}
