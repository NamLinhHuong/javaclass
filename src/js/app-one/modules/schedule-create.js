import React, { Component } from 'react';
import { Segment, Form, Checkbox, Dropdown } from 'semantic-ui-react'
import { getQueryString, updateQueryString, axiosGet, axiosPost, copyToClipboard } from '../../utils'
import { MyContext } from '../lazy'
import UtilsChosePitchChild from './utils-chose-pitch-child'

export default class ScheduleCreate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            expand: false,
        }
        this.handleChange = this.handleChange.bind(this);
        this.create = this.create.bind(this);
        this.genChecked = this.genChecked.bind(this);
        this.genInput = this.genInput.bind(this);
        this.getTeamOptions = this.getTeamOptions.bind(this);
        this.expandSwitch = this.expandSwitch.bind(this);
    }

    handleChange(event, { name, value }, storage) {
        storage.scheduleCreate.request = {...storage.scheduleCreate.request, [name]: event.target.value};
        storage.updateStorage(storage);
    }

    expandSwitch(event, data, storage) {
        this.state.expand = data.checked;
        storage.updateStorage(storage);
    }

    create(storage) {
        axiosPost('/api/san-ball-7/schedule/create?pitchChildId='
            + storage.scheduleCreate.pitchChildId,
            storage.scheduleCreate.request,
            function(response) {
                    storage.scheduleCreate.response = response;
                    storage.updateStorage(storage);
                }, function(response) {
                    console.log(response);
                }
            );
    }

    genChecked(storage, text, request, name) {
        return (
            <Form.Field>
                <Checkbox label={text} checked={request[name]}
                    onChange={(event, data) => {
                        request[name] = data.checked;
                        storage.updateStorage(storage);
                    }}/>
            </Form.Field>
        )
    }

    genInput(storage, text, request, name) {
        return (
            <Form.Field>
                <Form.Input label={text} name={[name]}
                    value={storage.scheduleCreate.request[name]}
                    onChange={(event, data) =>
                        this.handleChange(event, data, storage)} />
            </Form.Field>
        )
    }

    getTeamOptions(storage) {
        let list = storage.teamList.response.content;
        const options = _.map(list, (item, index) => ({
            key: item.id,
            text: item.name,
            value: item.id,
        }))
        return options;
    }

    render() {
        return (
            <MyContext.Consumer>
                {({storage}) => (
                    <div>
                        <Form>
                            <Form.Field>
                                <Checkbox label='Expand?'
                                    onChange={(event, data) => this.expandSwitch(event, data, storage)}/>
                            </Form.Field>
                            {this.state.expand && this.genChecked(storage, 'Booking?', storage.scheduleCreate.request, 'booking')}
                            {this.state.expand && this.genChecked(storage, 'Valid?', storage.scheduleCreate.request, 'valid')}
                            {this.state.expand && this.genChecked(storage, 'monday', storage.scheduleCreate.request, 'monday')}
                            {this.state.expand && this.genChecked(storage, 'tuesday', storage.scheduleCreate.request, 'tuesday')}
                            {this.state.expand && this.genChecked(storage, 'wednesday', storage.scheduleCreate.request, 'wednesday')}
                            {this.state.expand && this.genChecked(storage, 'thursday', storage.scheduleCreate.request, 'thursday')}
                            {this.state.expand && this.genChecked(storage, 'friday', storage.scheduleCreate.request, 'friday')}
                            {this.state.expand && this.genChecked(storage, 'saturday', storage.scheduleCreate.request, 'saturday')}
                            {this.state.expand && this.genChecked(storage, 'sunday', storage.scheduleCreate.request, 'sunday')}
                            {this.genInput(storage, 'fromYear', storage.scheduleCreate.request, 'fromYear')}
                            {this.genInput(storage, 'fromMonth', storage.scheduleCreate.request, 'fromMonth')}
                            {this.genInput(storage, 'fromDay', storage.scheduleCreate.request, 'fromDay')}
                            {this.genInput(storage, 'fromHour', storage.scheduleCreate.request, 'fromHour')}
                            {this.genInput(storage, 'fromMinute', storage.scheduleCreate.request, 'fromMinute')}
                            {this.genInput(storage, 'timezone', storage.scheduleCreate.request, 'timezone')}
                            {this.genInput(storage, 'percentInteger', storage.scheduleCreate.request, 'percentInteger')}
                            {this.genInput(storage, 'price', storage.scheduleCreate.request, 'price')}
                            {this.genInput(storage, 'currency', storage.scheduleCreate.request, 'currency')}
                            {this.state.expand && this.genInput(storage, 'toYear', storage.scheduleCreate.request, 'toYear')}
                            {this.state.expand && this.genInput(storage, 'toMonth', storage.scheduleCreate.request, 'toMonth')}
                            {this.state.expand && this.genInput(storage, 'toDay', storage.scheduleCreate.request, 'toDay')}
                            {this.state.expand && this.genInput(storage, 'toHour', storage.scheduleCreate.request, 'toHour')}
                            {this.state.expand && this.genInput(storage, 'toMinute', storage.scheduleCreate.request, 'toMinute')}
                            <UtilsChosePitchChild localStorage={storage.scheduleCreate}/>
                            <Form.Field>
                                <Dropdown name='teamId' placeholder='Team ID' search selection
                                    options={this.getTeamOptions(storage)}
                                    onChange={(event, data) => {
                                        storage.scheduleCreate.request.teamId = data.value;
                                        storage.updateStorage(storage);}}/>
                            </Form.Field>
                            <Form.Group>
                                <Form.Field>
                                <Form.Button content='Create Schedule'
                                    onClick={() => this.create(storage)} />
                                </Form.Field>
                            </Form.Group>
                        </Form>
                        <strong>request:</strong>
                        <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                            <pre>{JSON.stringify(storage.scheduleCreate.request, null, 2)} </pre>
                        </Segment>
                        <strong>response:</strong>
                        <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                            <pre>{JSON.stringify(storage.scheduleCreate.response.data, null, 2)} </pre>
                        </Segment>
                    </div>
                )}
            </MyContext.Consumer>
        )
    }
}
