import React, { Component } from 'react';
import { Segment, Form, Dropdown } from 'semantic-ui-react'
import { getQueryString, updateQueryString, axiosGet, axiosPost, copyToClipboard } from '../../utils'
import { MyContext } from '../lazy'
import _ from 'lodash'

export default class TeamAddMember extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.getOptions = this.getOptions.bind(this);
        this.getUserIds = this.getUserIds.bind(this);
        this.create = this.create.bind(this);
    }

    handleChange(event, { name, value }, storage) {
        storage.teamAddMember.request = {...storage.teamAddMember.request, [name]: event.target.value};
        storage.updateStorage(storage);
    }

    create(storage) {
        axiosPost('/api/san-ball-7/team/members/add?teamId=' + storage.teamAddMember.teamId,
            storage.teamAddMember.userIds,
            function(response) {
                storage.teamAddMember.response = response;
                storage.updateStorage(storage);
            },
            function(response) {
                console.log(response);
            }
        )
    }

    getOptions(storage) {
        let list = storage.teamList.response.content;
        const options = _.map(list, (item, index) => ({
            key: item.id,
            text: item.name,
            value: item.id,
        }))
        return options;
    }

    getUserIds(storage) {
        let list = storage.userList.response.content;
        const options = _.map(list, (item, index) => ({
            key: item.id,
            text: item.account,
            value: item.id,
        }))
        return options;
    }

    render() {
        return (
            <MyContext.Consumer>
                {({storage}) => (
                    <div>
                        <Form>
                            <Form.Group>
                                <Form.Field width={4}>
                                <Dropdown name='teamId' placeholder='Team Name' search selection
                                    options={this.getOptions(storage)}
                                    onChange={(event, data) => {
                                        storage.teamAddMember.teamId = data.value;
                                        storage.updateStorage(storage)}}/>
                                </Form.Field>
                                <Form.Field width={12}>
                                <Dropdown name='userIds' placeholder='User Id' fluid multiple selection
                                    options={this.getUserIds(storage)}
                                    onChange={(event, data) => {
                                        storage.teamAddMember.userIds = data.value;
                                        storage.updateStorage(storage)}}/>
                                </Form.Field>
                            </Form.Group>
                            <Form.Group>
                                <Form.Field>
                                <Form.Button content='Add Member For Team'
                                    onClick={() => this.create(storage)} />
                                </Form.Field>
                            </Form.Group>
                        </Form>
                        <strong>request:</strong>
                        <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                            <pre>{JSON.stringify(storage.teamAddMember.userIds, null, 2)} </pre>
                        </Segment>
                        <strong>response:</strong>
                        <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                            <pre>{JSON.stringify(storage.teamAddMember.response, null, 2)} </pre>
                        </Segment>
                    </div>
                )}
            </MyContext.Consumer>
        )
    }
}
