import React, { Component } from 'react';
import { Segment, Form, Dropdown } from 'semantic-ui-react'
import { getQueryString, updateQueryString, axiosGet, axiosPost, copyToClipboard } from '../../utils'
import { MyContext } from '../lazy'

export default class PitchAllService extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.getOptions = this.getOptions.bind(this);
        this.list = this.list.bind(this);
    }

    handleChange(event, { name, value }, storage) {
        storage.pitchAllService = {...storage.pitchAllService, [name]: event.target.value};
        storage.updateStorage(storage);
    }

    list(storage) {
        axiosGet('/api/san-ball-7/pitch/all-service?pitchId='
                    + storage.pitchAllService.pitchId, null,
            function(response){
                storage.pitchAllService.response = response.data;
                storage.updateStorage(storage);
            }, function(response) {
                console.log(response);
            });
    }

    getOptions(storage) {
        let list = storage.pitchList.response.content;
        const options = _.map(list, (item, index) => ({
            key: item.id,
            text: item.name,
            value: item.id,
        }))
        return options;
    }

    render() {
        return (
            <MyContext.Consumer>
                {({storage}) => (
                    <div>
                        <Form>
                            <Form.Group>
                                <Form.Field>
                                    <Dropdown name='pitchId' placeholder='Pitch ID' search selection
                                        options={this.getOptions(storage)}
                                        onChange={(event, data) => {
                                            storage.pitchAllService.pitchId = data.value;
                                            storage.updateStorage(storage)}}/>
                                </Form.Field>
                            </Form.Group>
                            <Form.Group>
                                <Form.Button content='Get Services Of Pitch'
                                    onClick={() => this.list(storage)} />
                                <Form.Button content='Copy'
                                    onClick={() => copyToClipboard(JSON.stringify(storage.pitchAllService.response))} />
                            </Form.Group>
                        </Form>
                        <strong>response:</strong>
                        <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                            <pre> {JSON.stringify(storage.pitchAllService.response, null, 2)} </pre>
                        </Segment>
                    </div>
                )}
            </MyContext.Consumer>
        )
    }
}
