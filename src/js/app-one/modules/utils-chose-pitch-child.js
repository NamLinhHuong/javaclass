import React, { Component } from 'react';
import { Segment, Form, Dropdown } from 'semantic-ui-react'
import { getQueryString, updateQueryString, axiosGet, copyToClipboard } from '../../utils'
import { MyContext } from '../lazy'

export default class UtilsChosePitchChild extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
        this.getOptions = this.getOptions.bind(this);
        this.getChildOptions = this.getChildOptions.bind(this);
        this.loadPitchChild = this.loadPitchChild.bind(this);
    }

    getOptions(storage) {
        let list = storage.pitchList.response.content;
        const options = _.map(list, (item, index) => ({
            key: item.id,
            text: item.name,
            value: item.id,
        }))
        return options;
    }

    getChildOptions(storage, localStorage) {
        let list = localStorage.pitchChildSet;
        const options = _.map(list, (item, index) => ({
            key: item.id,
            text: item.name,
            value: item.id,
        }))
        return options;
    }

    loadPitchChild(pitchId, storage, localStorage) {
        axiosGet('/api/san-ball-7/pitch?pitchId='+pitchId, null,
            function(response) {
                localStorage.pitchId = pitchId;
                localStorage.pitchChildSet = response.data.content.pitchChildSet;
                storage.updateStorage(storage);
            }, function(response) {
                console.log(response);
            });
    }

    render() {
        let localStorage = this.props.localStorage;
        return (
            <MyContext.Consumer>
                {({storage, updateStorage}) => (
                    <Form.Group>
                        <Form.Field>
                        <Dropdown name='pitchId' placeholder='Pitch' search selection
                            options={this.getOptions(storage)}
                            onChange={(event, data) => {
                                this.loadPitchChild(data.value, storage, localStorage)}}/>
                        </Form.Field>
                        <Form.Field>
                        <Dropdown name='pitchChildId' placeholder='Pitch Child' search selection
                            options={this.getChildOptions(storage, localStorage)}
                            onChange={(event, data) => {
                                localStorage.pitchChildId = data.value;
                                storage.updateStorage(storage)}}/>
                        </Form.Field>
                    </Form.Group>
                )}
            </MyContext.Consumer>
        )
    }
}
