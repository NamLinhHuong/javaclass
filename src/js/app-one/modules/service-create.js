import React, { Component } from 'react';
import { Segment, Form, Dropdown } from 'semantic-ui-react'
import { getQueryString, updateQueryString, axiosGet, axiosPost, copyToClipboard } from '../../utils'
import { MyContext } from '../lazy'

export default class ServiceCreate extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.getOptions = this.getOptions.bind(this);
        this.create = this.create.bind(this);
    }

    handleChange(event, { name, value }, storage) {
        storage.serviceCreate.request = {...storage.serviceCreate.request, [name]: event.target.value};
        storage.updateStorage(storage);
    }

    create(storage) {
        axiosPost('/api/san-ball-7/pitch/service/create?pitchId=' + storage.serviceCreate.pitchId,
            storage.serviceCreate.request,
            function(response) {
                    storage.serviceCreate.response = response;
                    storage.updateStorage(storage);
                }, function(response) {
                    console.log(response);
                }
            );
    }

    getOptions(storage) {
        let list = storage.pitchList.response.content;
        const options = _.map(list, (item, index) => ({
            key: item.id,
            text: item.name,
            value: item.id,
        }))
        return options;
    }

    render() {
        return (
            <MyContext.Consumer>
                {({storage}) => (
                    <div>
                        <Form>
                            <Form.Group>
                                <Form.Field width={8}>
                                <Form.Input label='name' name='name'
                                    value={storage.serviceCreate.request.name}
                                    onChange={(event, data) =>
                                        this.handleChange(event, data, storage)} />
                                </Form.Field>
                                <Form.Field width={8}>
                                <Form.Input label='description' name='description'
                                    value={storage.serviceCreate.request.description}
                                    onChange={(event, data) =>
                                        this.handleChange(event, data, storage)} />
                                </Form.Field>
                            </Form.Group>
                            <Form.Group>
                                <Form.Field>
                                <Dropdown name='pitchId' placeholder='Pitch ID' search selection
                                    options={this.getOptions(storage)}
                                    onChange={(event, data) => {
                                        storage.serviceCreate.pitchId = data.value;
                                        storage.updateStorage(storage)}}/>
                                </Form.Field>
                            </Form.Group>
                            <Form.Group>
                                <Form.Field>
                                <Form.Button content='Create Service'
                                    onClick={() => this.create(storage)} />
                                </Form.Field>
                            </Form.Group>
                        </Form>
                        <strong>request:</strong>
                        <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                            <pre>{JSON.stringify(storage.serviceCreate.request, null, 2)} </pre>
                        </Segment>
                        <strong>response:</strong>
                        <Segment style={{ overflow: 'auto', maxHeight: '27em' }}>
                            <pre>{JSON.stringify(storage.serviceCreate.response.data, null, 2)} </pre>
                        </Segment>
                    </div>
                )}
            </MyContext.Consumer>
        )
    }
}
