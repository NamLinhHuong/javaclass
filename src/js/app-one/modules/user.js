import React, { Component } from 'react';
import { Segment, Form } from 'semantic-ui-react'
import { getQueryString, updateQueryString, axiosGet, copyToClipboard } from '../../utils'
import { MyContext } from '../lazy'
import CurrentUserManager from './user-current'
import UserListManager from './user-list'

export default class UserManager extends Component {
    constructor(props) {
        super(props);
        this.loadUserList = this.loadUserList.bind(this);
    }

    componentWillUnmount() {
    }

    componentDidMount() {
    }

    handleChangeUser(event) {
        this.setState({user: event.target.value});
    }
    handleChangeMessage(event) {
        this.setState({message: event.target.value});
    }

    loadUserList(storage, updateStorage) {
        let self = this
        axiosGet('/api/san-ball-7/user/list', null,
            function(response){
                storage.userListResponse = response;
                storage.userList = response.data.content;
                updateStorage(storage);
            }, function(response) {
                console.log(response);
            }
        );
    }

    render() {
        // console.log(JSON.parse(JSON.stringify(questionGlobal)));
        return (
            <MyContext.Consumer>
                {({storage, updateStorage}) => (
                    <Segment basic>
                        <Segment.Group>
                            <Segment><CurrentUserManager/></Segment>
                            <Segment><UserListManager/></Segment>
                        </Segment.Group>
                    </Segment>
                )}
            </MyContext.Consumer>
        )
    }
}
