import React, { Component, Suspense } from 'react';
import LazyAppOne from './lazy';
//const LazyAppOne = React.lazy(() => import('./lazy'));

export default class AppOne extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
        this.insertLibs = this.insertLibs.bind(this);
        this.removeLibs = this.removeLibs.bind(this);
    }

    componentWillUnmount() {
        this.props.toggleHome();
        this.removeLibs();
    }

    componentDidMount() {
        this.props.toggleHome();
        this.insertLibs();
    }

    insertLibs() {
        let head = document.getElementsByTagName('head')[0];
        let link = document.createElement('link');
        link.rel = 'stylesheet';
//        link.href = 'https://unpkg.com/purecss@1.0.0/build/pure-min.css';
//        link.integrity = "sha384-nn4HPE8lTHyVtfCBi5yW9d20FjT8BJwUXyWZT9InLYax14RDjBj46LmSztkmNP9w";
//        link.crossOrigin = "anonymous";
        link.href = '//cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css';
        link.id = 'app-one-link';
        console.log(link);
        head.appendChild(link);
    }

    removeLibs() {
        document.getElementById('app-one-link').outerHTML = '';
    }

    handleChangeUser(event) {
        this.setState({user: event.target.value});
    }
    handleChangeMessage(event) {
        this.setState({message: event.target.value});
    }

    render() {
        return (
            <LazyAppOne closeApp={this.props.closeApp}/>
        )
//        return (
//            <div>
//              <Suspense fallback={<div>Loading...</div>}>
//                <LazyAppOne closeApp={this.props.closeApp}/>
//              </Suspense>
//            </div>
//        )
    }
}
