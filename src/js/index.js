import React from 'react';
import ReactDOM from 'react-dom';
import Login from './login';
import Home from './home';
import {updateAuthCookie} from './utils'

updateAuthCookie();

let app = (<Login/>);
if (document.getElementById('index')) {
    app = (<Home/>);
}
ReactDOM.render(
    app, //<div>{title}</div>,
    document.getElementById('app')
);

module.hot.accept();