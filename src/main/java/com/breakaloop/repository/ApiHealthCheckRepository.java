package com.breakaloop.repository;

import com.breakaloop.entity.ApiHealthCheck;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApiHealthCheckRepository extends JpaRepository<ApiHealthCheck, String> {
}
