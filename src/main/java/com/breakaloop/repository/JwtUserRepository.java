package com.breakaloop.repository;

import com.breakaloop.entity.JwtUser;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Carlos
 */
public interface JwtUserRepository extends CrudRepository<JwtUser,String> {
    
    public JwtUser findByFacebook(String id);
    
    public JwtUser findByGoogle(String id);
    
    public JwtUser findByEmail(String email);
}
