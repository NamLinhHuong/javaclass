package com.breakaloop.config;

import com.breakaloop.constant.Constants;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;


@Configuration
@Profile(Constants.SPRING_PROFILE_PRODUCTION)
public class ProdRedisConfig {
    private static Logger logger = Logger.getLogger(ProdRedisConfig.class);

    @Value("${redis.uri}")
    private String redisUri;

    @Bean
    public JedisConnectionFactory jedisConnFactory() throws URISyntaxException {

        // host: ec2-35-168-150-175.compute-1.amazonaws.com
        // user: h
        // port: 39229
        // pass: pe5839fe1312554dac737b63940ae6305bb44d1bab79594055ab546fd635ffd4c
        // uri: redis://h:pe5839fe1312554dac737b63940ae6305bb44d1bab79594055ab546fd635ffd4c@ec2-35-168-150-175.compute-1.amazonaws.com:39229

        try {
            String redistogoUrl = System.getenv("REDISTOGO_URL");
            redistogoUrl = redistogoUrl == null ? redisUri : redistogoUrl;
            logger.debug("redis:" + redistogoUrl);
            URI redistogoUri = new URI(redistogoUrl);

            RedisStandaloneConfiguration redisStandaloneConfiguration =
                    new RedisStandaloneConfiguration(redistogoUri.getHost(), redistogoUri.getPort());
            redisStandaloneConfiguration.setPassword(RedisPassword.of(redistogoUri.getUserInfo().split(":", 2)[1]));

            JedisClientConfiguration.JedisClientConfigurationBuilder jedisClientConfiguration = JedisClientConfiguration.builder();
            jedisClientConfiguration.connectTimeout(Duration.ofSeconds(60));// 60s connection timeout

            return new JedisConnectionFactory(redisStandaloneConfiguration,
                    jedisClientConfiguration.build());

        } catch (URISyntaxException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }
}