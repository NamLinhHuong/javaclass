package com.breakaloop.config;

import com.google.common.base.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Set;

// http://localhost:8080/v2/api-docs
// http://localhost:8080/swagger-ui.html#/user45profile45controller

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                //.apis(RequestHandlerSelectors.any())
                .apis(customRequestHandlers())
                .paths(PathSelectors.any())
                .build();
    }

    private Predicate<RequestHandler> customRequestHandlers() {
        return new Predicate<RequestHandler>() {
            @Override
            public boolean apply(RequestHandler requestHandler) {
                Set<RequestMethod> methods = requestHandler.supportedMethods();
                //RequestMethod next = methods.iterator().next();
                return true;
//                return methods.contains(RequestMethod.GET)
//                        || methods.contains(RequestMethod.POST)
//                        || methods.contains(RequestMethod.PUT)
//                        || methods.contains(RequestMethod.DELETE);
            }
        };
    }
}