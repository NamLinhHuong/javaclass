package com.breakaloop.config;

import com.breakaloop.dto.Book;
import com.breakaloop.helper.JsonRedisSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import javax.inject.Inject;

@Configuration
public class RedisConfig {

    @Inject
    private JedisConnectionFactory jedisConnFactory;

    @Bean
    public StringRedisSerializer stringRedisSerializer() {
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        return stringRedisSerializer;
    }

//    @Bean
//    public JacksonJsonRedisSerializer<Book> jacksonJsonRedisJsonSerializer() {
//        JacksonJsonRedisSerializer<Book> jacksonJsonRedisJsonSerializer = new JacksonJsonRedisSerializer<>(Book.class);
//        return jacksonJsonRedisJsonSerializer;
//    }

    @Bean
    public JsonRedisSerializer jacksonJsonRedisJsonSerializer() {
        return new JsonRedisSerializer();
    }

    @Bean
    public RedisTemplate<String, Book> redisTemplate() {
        RedisTemplate<String, Book> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(jedisConnFactory);
        redisTemplate.setKeySerializer(stringRedisSerializer());
        redisTemplate.setValueSerializer(jacksonJsonRedisJsonSerializer());
        return redisTemplate;
    }
}