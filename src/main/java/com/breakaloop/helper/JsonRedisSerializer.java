package com.breakaloop.helper;

import org.apache.commons.lang3.ArrayUtils;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

import javax.xml.ws.Holder;

public class JsonRedisSerializer implements RedisSerializer<Object> {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    static {
        OBJECT_MAPPER.enableDefaultTyping();
        OBJECT_MAPPER.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Override
    public Object deserialize(byte[] bytes) throws SerializationException {
        if (ArrayUtils.isEmpty(bytes)) {
            return null;
        }
        try {
            return OBJECT_MAPPER.readValue(bytes, Holder.class).value;//.getValue();
        } catch (Exception e) {
            throw new SerializationException("Error on converting bytearray to json object", e);
        }
    }

    @Override
    public byte[] serialize(Object t) throws SerializationException {
        if (t == null) {
            return new byte[0];
        }
        try {
            return OBJECT_MAPPER.writeValueAsBytes(new Holder(t));
        } catch (Exception e) {
            throw new SerializationException("Error on writing json object to bytearray", e);
        }
    }

}