package com.breakaloop.constant;

public class Constants {
    // Authentication
    public static final String CONFLICT_MSG = "There is already a %s account that belongs to you",
            NOT_FOUND_MSG = "JwtUser not found", LOGING_ERROR_MSG = "Wrong account and/or password",
            UNLINK_ERROR_MSG = "Could not unlink %s account because it is your only sign-in method";

    // OAuth
    public static final String CLIENT_ID_KEY = "client_id", REDIRECT_URI_KEY = "redirect_uri",
            CLIENT_SECRET = "client_secret", CODE_KEY = "code", GRANT_TYPE_KEY = "grant_type",
            AUTH_CODE = "authorization_code", STATE = "state";
    public static final String SPRING_PROFILE_PRODUCTION = "dev";

    // "yyyy.MM.dd G 'at' HH:mm:ss z"	        2001.07.04 AD at 12:08:56 PDT
    // "EEE, MMM d, ''yy"	                    Wed, Jul 4, '01
    // "h:mm a"	                                12:08 PM
    // "hh 'o''clock' a, zzzz"	                12 o'clock PM, Pacific Daylight Time
    // "K:mm a, z"	                            0:08 PM, PDT
    // "yyyyy.MMMMM.dd GGG hh:mm aaa"	        02001.July.04 AD 12:08 PM
    // "EEE, d MMM yyyy HH:mm:ss Z"	            Wed, 4 Jul 2001 12:08:56 -0700
    // "yyMMddHHmmssZ"	                        010704120856-0700
    // "yyyy-MM-dd'T'HH:mm:ss.SSSZ"             2001-07-04T12:08:56.235-0700
    // "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"	        2001-07-04T12:08:56.235-07:00
    // "YYYY-'W'ww-u"                           2001-W27-3
    // "yyyy-MM-dd HH:mm a z"
    public static final String FULL_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS XXX"; // 2019-02-15 19:05:00.000 +09:00
    //public static final String FULL_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"; // 2019-03-07T15:18:25.570+0700

    public static final String DATE_ONLY_FORMAT = "yyyy-MM-dd"; // 2019-02-15 19:05:00.000 +09:00

    public static final String JWT_TYPE = "JWT_TYPE";
}
