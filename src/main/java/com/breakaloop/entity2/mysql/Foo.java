package com.breakaloop.entity2.mysql;

import javax.persistence.*;

@Entity
@Table(name = "FOO")
public class Foo {

    @Id
    @GeneratedValue
    @Column(name = "BaseEntity")
    private Long id;

    @Column(name = "FOO")
    private String foo;

    Foo(String foo) {
        this.foo = foo;
    }

    Foo() {
        // Default constructor needed by JPA
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFoo() {
        return foo;
    }

    public void setFoo(String foo) {
        this.foo = foo;
    }
}