package com.breakaloop.service;


import com.breakaloop.entity.JwtUser;
import com.breakaloop.entity.User;

/**
 *
 * @author Carlos
 */
public interface JwtUserService {
    JwtUser findByFacebook(String id);
    JwtUser findByGoogle(String id);
    JwtUser findByEmail(String email);
    JwtUser findOne(String subject);
    JwtUser save(JwtUser jwtUser);

    void fulfillUser(User user);
}
