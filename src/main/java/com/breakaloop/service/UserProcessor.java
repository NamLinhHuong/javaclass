/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.breakaloop.service;

import com.nimbusds.jose.JOSEException;
import org.springframework.http.ResponseEntity;

import java.text.ParseException;

/**
 *
 * @author Carlos
 */
public interface UserProcessor {
    ResponseEntity processUser(final String authHeader, final String remoteHost, final String providerId,
                               final String id, final String displayName, final String email, final String picture,
                               final String name, final String givenName, final String familyName)
            throws JOSEException, ParseException;
}
