package com.breakaloop.service.impl.auth;

import com.breakaloop.helper.Token;
import com.breakaloop.service.SocialService;
import com.breakaloop.service.UserProcessor;
import com.nimbusds.jose.JOSEException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

@Service
public class SocialConnectionSignup implements ConnectionSignUp {

    @Autowired
    SocialService socialService;

    @Autowired
    private UserProcessor userProcessor;

    @Override
    public String execute(Connection<?> connection) {
        String remoteHost = "aaaaaa";
        String authHeader = "";

        String id = "<o>";

        try {
            System.out.println(userProcessor);

            ResponseEntity responseEntity = userProcessor.processUser(
                    authHeader, remoteHost, connection.getKey().getProviderId(),
                    connection.getKey().getProviderUserId(),
                    connection.getDisplayName(),
                    null,
                    connection.getImageUrl(),
                    connection.getDisplayName(),
                    null, // first name
                    null); // last name

            System.out.println(responseEntity.getStatusCodeValue());
            System.out.println(responseEntity.getStatusCode());
            System.out.println("aaaaaa");
            if (responseEntity.getBody() instanceof String) {
                System.out.println((String)responseEntity.getBody());
            } else if (responseEntity.getBody() instanceof Map) {

                Map<String, Token> map = (HashMap) responseEntity.getBody();

                for (Map.Entry<String, Token> entry : map.entrySet()) {
                    System.out.println(entry.getKey() + " ---> " + entry.getValue().getToken());
                    id = entry.getKey();
                }

            }
            System.out.println("aaaaaa");
        } catch (JOSEException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return id;
    }
}