package com.breakaloop.service.impl;

import com.breakaloop.entity.MonMan;
import com.breakaloop.repository.MonManRepository;
import com.breakaloop.service.MonManService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MonManServiceImpl implements MonManService {
    @Autowired
    MonManRepository monManRepository;
    @Override
    public MonMan save(MonMan mm) {
        return monManRepository.save(mm);
    }

    @Override
    public List<MonMan> findByUserId(String userId) {
        return monManRepository.findByUserId(userId);
    }

    @Override
    public MonMan findById(Long id) {
        Optional<MonMan> monMan = monManRepository.findById(id);
        return monMan.get();
    }

    @Override
    public void delete(MonMan monMan) {
        monManRepository.delete(monMan);
    }

    @Override
    public Page<MonMan> findPaginated(Pageable pageable) {
        return monManRepository.findAll(pageable);
    }
}
