package com.breakaloop.service.impl;

import com.breakaloop.entity.User;
import com.breakaloop.service.JwtUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public class AuditorAwareImpl implements AuditorAware<String> {

    @Autowired
    JwtUserService jwtUserService;

    @Override
    public Optional<String> getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        if (principal instanceof User) {
            User user = (User) principal;

            if (user.getUserId() == null) {
                jwtUserService.fulfillUser(user);
                if (user.getUserId() != null) {
                    Authentication newAuthentication = new UsernamePasswordAuthenticationToken(user, authentication.getCredentials(), authentication.getAuthorities());
                    SecurityContextHolder.getContext().setAuthentication(newAuthentication);
                }
            }

            if (user.getUserId() == null){
                return null;
            }

            return Optional.of(user.getUserId());
        } else {
            return Optional.of((String) principal);
        }
    }

    public static Optional<String> getCurrentUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        if (principal instanceof User) {
            User user = (User) principal;
            return Optional.of(user.getUserId());
        } else {
            return Optional.of((String) principal);
        }
    }
}
