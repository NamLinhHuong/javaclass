package com.breakaloop.service.impl;

import com.breakaloop.entity.JwtUser;
import com.breakaloop.entity.User;
import com.breakaloop.entity.UserConnection;
import com.breakaloop.helper.AuthUtils;
import com.breakaloop.helper.Token;
import com.breakaloop.repository.JwtUserRepository;
import com.breakaloop.repository.UserConnectionRepository;
import com.breakaloop.service.JwtUserService;
import com.nimbusds.jose.JOSEException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 *
 * @author Carlos
 */
@Service
public class JwtUserServiceImpl implements JwtUserService {
    @Autowired
    private JwtUserRepository jwtUserRepository;

    @Autowired
    UserConnectionRepository userConnectionRepository;

    @Override
    public JwtUser findByFacebook(String id) {
        return this.jwtUserRepository.findByFacebook(id);
    }

    @Override
    public JwtUser findByGoogle(String id) {
        return this.jwtUserRepository.findByGoogle(id);
    }

    @Override
    public JwtUser findByEmail(String email) {
        return this.jwtUserRepository.findByEmail(email);
    }

    @Override
    public JwtUser findOne(String subject) {
        //return this.jwtUserRepository.findOne(subject);
        return this.jwtUserRepository.findById(subject).get();
    }

    @Override
    public JwtUser save(JwtUser jwtUser) {
        return this.jwtUserRepository.save(jwtUser);
    }

    @Override
    public void fulfillUser(User user) {
        if (user.getUserId() == null && user.getProviderId() != null && user.getProviderUserId() != null) {
            UserConnection userConnection = userConnectionRepository.findByProviderIdAndUserProviderId(
                    user.getProviderId(), user.getProviderUserId());
            if (userConnection != null) {
                String id = userConnection.getUserConnectionIdentity().getUserId();
                if (id != null) {
                    user.setUserId(id);
                } else {
                    id = UUID.randomUUID().toString();

                    String remoteHost = "AAAAAAAAA"; // TODO:
                    try {
                        Token token = AuthUtils.createToken(remoteHost, id);
                    } catch (JOSEException e) {
                        e.printStackTrace();
                    }

                    user.setUserId(id);
                    userConnection.getUserConnectionIdentity().setUserId(id);
                    userConnectionRepository.save(userConnection);
                }
            }
        }
    }
}
