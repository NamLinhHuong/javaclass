package com.breakaloop.service;

import com.breakaloop.dto.Book;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service
public class BookService { // Redis not support @Transactional
    @Inject
    private BookRepository bookRepository;

    public List<Book> findAll() {
        return Lists.newArrayList(bookRepository.findAll());
    }

    public Book findById(String id) {
        return bookRepository.findById(id);
    }

    public void save(Book book) {
        bookRepository.save(book);
    }

    public void delete(Book book) {
        bookRepository.delete(book);
    }

    public void deleteAll() {
        bookRepository.deleteAll();
    }
}