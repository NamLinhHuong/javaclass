package com.breakaloop.service;

import com.nimbusds.jose.JOSEException;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.text.ParseException;

public interface SocialService {
    ResponseEntity login(String redirectUri, String code,
                         String authHeader, String remoteHost) throws ParseException, JOSEException, IOException;
    ResponseEntity loginV3(String redirectUri, String code, String state,
                           String authHeader, String remoteHost) throws ParseException, JOSEException, IOException;
}
