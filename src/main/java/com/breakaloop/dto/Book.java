package com.breakaloop.dto;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class Book implements Serializable {

    private static final long serialVersionUID = 285628042347903029L;

    private static long longId = 100;

    private String id;

    @NotEmpty
    @Size(min = 5, max = 100)
    private String name;

    @NotEmpty
    private String publisher;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private DateTime dateOfPublication;

    private String description;

    private String photo;

    public Book() {

    }

    public Book(String id) {
        this.id = id;
    }

    public static String generateNextId() {
        return String.valueOf(longId++);
    }

    public static long getLongId() {
        return longId;
    }

    public static void setLongId(long longId) {
        Book.longId = longId;
    }

    public static void resetId() {
        longId = 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public DateTime getDateOfPublication() {
        return dateOfPublication;
    }

    public void setDateOfPublication(DateTime dateOfPublication) {
        this.dateOfPublication = dateOfPublication;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}