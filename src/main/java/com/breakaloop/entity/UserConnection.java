package com.breakaloop.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;

@Entity
@Table(name = "userconnection")
public class UserConnection {
    @EmbeddedId
    UserConnectionIdentity userConnectionIdentity;
    @NotNull
    @Column(name = "rank")
    Integer rank;
    @Column(name = "displayname")
    String displayName;
    @Column(name = "profileurl")
    String profileUrl;
    @Column(name = "imageurl")
    String imageUrl;
    @Column(name = "accesstoken", nullable = false)
    String accessToken;
    @Column(name = "secret")
    String secret;
    @Column(name = "refreshtoken")
    String refreshToken;
    @Column(name = "expiretime")
    BigInteger expireTime;

    public UserConnectionIdentity getUserConnectionIdentity() {
        return userConnectionIdentity;
    }

    public void setUserConnectionIdentity(UserConnectionIdentity userConnectionIdentity) {
        this.userConnectionIdentity = userConnectionIdentity;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public BigInteger getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(BigInteger expireTime) {
        this.expireTime = expireTime;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }
}
