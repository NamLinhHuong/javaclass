package com.breakaloop.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Entity
public class MyUser implements Serializable {
    private static final long serialVersionUID = 0x62A6DA99AABDA8A8L;
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Integer userId;
    @Column
    private String userName;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Skill> skills= new LinkedList<>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY) // no more than on EAGER or use SET instead ?????
    private List<Test> tests= new LinkedList<>();


    public Integer getUserId() {
        return userId;
    }
    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public List<Skill> getSkills() {
        return skills;
    }
    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }
    public MyUser() {
    }
    public MyUser(String userName, List<Skill> skills) {
        this.userName = userName;
        this.skills = skills;

        this.tests = new ArrayList<>();
        Test test = new Test();
        test.setMyUser(this);
        test.setSkillName("ijiji" + Math.random());
        this.tests.add(test);
    }

    public List<Test> getTests() {
        return tests;
    }

    public void setTests(List<Test> tests) {
        this.tests = tests;
    }
}