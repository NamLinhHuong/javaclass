package com.breakaloop.entity;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
@Builder
@ToString
@EqualsAndHashCode
public class User implements Serializable {
    private static final long serialVersionUID = -8757266715072872695L;
    String userId; // gen at SocialConnectionSignup = UUID
    String providerId; // facebook, google
    String providerUserId;
}
