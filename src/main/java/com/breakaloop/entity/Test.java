package com.breakaloop.entity;

import javax.persistence.*;

@Entity
public class Test extends Auditable<String> {
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Integer skillId;
    @Column
    private String skillName;
    @ManyToOne
    private MyUser myUser;


    public Test(String skillName) {
        this.skillName = skillName;
    }
    public Integer getSkillId() {
        return skillId;
    }
    public void setSkillId(Integer skillId) {
        this.skillId = skillId;
    }
    public String getSkillName() {
        return skillName;
    }
    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }
    public MyUser getMyUser() {
        return myUser;
    }
    public void setMyUser(MyUser myUser) {
        this.myUser = myUser;
    }
    public Test() {
    }
    public Test(String skillName, MyUser myUser) {
        this.skillName = skillName;
        this.myUser = myUser;
    }
}
