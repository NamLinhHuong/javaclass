package com.breakaloop.entity;

import com.breakaloop.helper.BaseEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity(name="api_health_check")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiHealthCheck extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -7866191418797920653L;

    @Column(name = "trigger_date")
    Date triggerDate;
    @Column
    String ip;
}
