package com.breakaloop.javaclass.ngoc;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class Ngoc2_2{
    public static void main(String []args){
        System.out.println("Nhập n");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        List<Integer> array = new ArrayList<Integer>();
        if(n < 2) {
            System.out.println("Không có số nguyên tố");
        }else{
            for(int i = 2; i <= n; i++){
                if(checkNT(i)){
                    array.add(i);
                }
            }
            System.out.print("Dãy số nguyên tố :");
            for(int i = 0; i < array.size(); i++){
                System.out.print(" " + array.get(i));
            }
        }

    }


    public static boolean checkNT(int a){
        int squareRoot = (int) Math.sqrt(a);
        for (int i = 2; i <= squareRoot; i++) {
            if (a % i == 0) {
                return false;
            }
        }
        return true;
    }
}