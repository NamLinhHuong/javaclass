package com.breakaloop.javaclass.hang;

import java.util.Random;

public class ThreeThreads {
    int i = 0;
    int j = 0;
    static Thread1 t1;
    static Thread2 t2;
    static Thread3 trThread3;
    public static void main(String[] args) {
        ThreeThreads threeThreads = new ThreeThreads();
        t1 = threeThreads.new Thread1();
        t2 = threeThreads.new Thread2();
        trThread3 = threeThreads.new Thread3();
        t1.start();
        t2.start();
        trThread3.start();
        return;
    }
    public class Thread1 extends Thread {
        public Thread1() {
        }
        public int getSleepTime() {
            return 1000;
        }
        public void run() {
            try {
                for (;;) {
                    i = ThreeThreads.randomnumber(0, 5);
                    System.out.println("value i: " + i);
                    Thread.sleep(getSleepTime());
                }
            } catch (InterruptedException e) {
                System.out.println(i + "Interrupted");
            }
        }
    }
    public class Thread2 extends Thread {
        public void run() {
            try {
                for (;;) {
                    j = ThreeThreads.randomnumber(0, 5);
                    System.out.println("value j: " + j);
                    Thread.sleep(2000);
                }
            } catch (InterruptedException e) {
                System.out.println(j + "Interrupted");
            }
        }
    }
    public class Thread3 extends Thread {
        public void run() {
            try {
                for (;;) {
                    if (i == j) {
                        t1.interrupt();
                        t2.interrupt();
                        break;
                    }
                    Thread.sleep(1500);
                }
            } catch (InterruptedException e) {
                System.out.println("Interrupted");
            }
        }
    }
    public static int randomnumber(int low, int high) {
        Random r = new Random();
        return r.nextInt(high - low) + low;
    }
}