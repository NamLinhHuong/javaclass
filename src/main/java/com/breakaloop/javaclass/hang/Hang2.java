package com.breakaloop.javaclass.hang;

public class Hang2 {
    public static void main(String[] args) {
        MyThread a1 = new MyThread();
        a1.start();
        try{
            a1.sleep(500);
        }
        catch(Exception e){
            e.printStackTrace();
        };
        MyThreadNext a2 = new MyThreadNext();
        a2.start();
    }
    public static class MyThread extends Thread{
        int a = 0;
        public void run(){
            for (int i = 0; i < 10; i++) {
                a = a+1;
                System.out.println(a);
            }
        }
    }
    public static class MyThreadNext extends Thread{
        int a = 0;
        public void run(){
            for (int i = 0; i <10;i++){
                a = a-2;
                System.out.println(a);
            }
        }
    }
}
