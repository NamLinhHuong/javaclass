package com.breakaloop.javaclass.hang;
import java.util.ArrayList;
import java.util.Scanner;

class AddedBook {
    private String id = "";
    private String bookName ="";
    public AddedBook(String isbn, String name) {
// TODO Auto-generated constructor stub
        this.id = isbn;
        this.bookName = name;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getBookName() {
        return bookName;
    }
    public void setBookName(String bookName) {
        this.bookName = bookName;
    }
    public String toString(){
        return "id:"+id+";"+"name:"+bookName;
    }
}

class book{
    static String isbn = "";
    static String name = "";
    private static Scanner typeBook;
    public static ArrayList<AddedBook> listBooks;
    public static AddedBook objBook;
    public static Scanner findBook;
    public static String inputBookName;
    public static void main (String[]args){
        insertBook();
        searchBookName();
    }
    public static void searchBookName() {
// TODO Auto-generated method stub
        System.out.println("Input the name of book you want to find:");
        findBook = new Scanner(System.in);
        inputBookName = findBook.nextLine();
        System.out.println("Search: " + inputBookName);
        for (int i = 0; i < listBooks.size(); i++) {
            if (inputBookName.equals(listBooks.get(i).getBookName()) ) {
                System.out.println(listBooks.get(i).toString());
            }
        }
    }
    public static void insertBook(){
        for (int i = 0; i < 2; i++) {
            System.out.println("Input the book number"+i);
            typeBook = new Scanner(System.in);
            isbn = typeBook.nextLine();
            name = typeBook.nextLine();
            listBooks = new ArrayList<AddedBook>();
            objBook = new AddedBook(isbn,name);
            System.out.println("Added: " + objBook.toString());
            listBooks.add(objBook);
        }
    }
}


public class ThienLam {
    public static void main(String[] args) {
        book.main(args);
    }
}
