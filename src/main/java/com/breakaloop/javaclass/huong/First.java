package com.breakaloop.javaclass.huong;


class Item {
    Item previous;
    Object value;
    Item next;
}

class MyLinkedList {
    private Object[] object = null;

    public MyLinkedList(){

    }

    //tạo 1 object mới, coppy object cũ
    public void add(int index, Object element){
        try{
            if(object == null){
                object = new Object[]{element};
            }else{
                Object[] tmp = new Object[object.length + 1];
                for(int i = 0; i< tmp.length; i++){
                    if(i < index){
                        tmp[i] = object[i];
                    }else if (i == index){
                        tmp[i] = element;
                    }else{
                        tmp[i] = object[i - 1];
                    }
                }
                object = tmp;
            }
        }catch(IndexOutOfBoundsException e){
            System.out.println(e);
        }
    }

    public boolean add(Object element){
        if(object == null){
            object = new Object[]{element};
        }else{
            Object[] tmp = new Object[object.length + 1];
            for(int i =0; i < object.length; i++){
                tmp[i] = object[i];
            }
            tmp[object.length] = element;
            object = tmp;
        }
        return true;
    }

    public void clear(){
        object = null;
    }

    public Object get(int index){
        try{
            return object[index];
        }catch(IndexOutOfBoundsException  e) {
            System.out.println(e);
        }
        return null;
    }

    public int indexOf(Object o){
        for(int i = 0; i < object.length; i++){
            if(object[i].equals(o)){
                return i;
            }
        }
        return -1;
    }

    public Object remove(int index){
        Object[] tmp = new Object[object.length -1];
        for(int i = 0; i < object.length -1 ; i++){
            if(i < index) {
                tmp[i] = object[i];
            }else if(i >= index){
                tmp[i] = object[i+1];
            }
        }
        return object = tmp;
    }

    public void println(){
        for(int i = 0; i < object.length; i++){
            System.out.println(object[i]);
        }
    }
    public int size(){
        return object.length;
    }

}



public class First{

    public static void main(String[] args){
        MyLinkedList a = new MyLinkedList();
        a.add("134");
        a.add("120");
        a.add("43");
        a.clear();
        a.add("143425");
        a.add("ụy");
        a.add("ỵbgdrf");
        a.add("rdrh");
        a.add(2,"121");
        a.remove(3);
        a.println();
        System.out.println(a.size());
    }
}


