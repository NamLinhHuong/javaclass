package com.breakaloop.app.quartzjob;

import com.breakaloop.app.quartzjob.entity.JobGeneric;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface JobGenericRepository extends JpaRepository<JobGeneric, byte[]> {
    @Query("SELECT jobGeneric FROM JobGeneric jobGeneric WHERE jobGeneric.expiredDate is not null and jobGeneric.expiredDate >= CURRENT_TIMESTAMP")
    List<JobGeneric> findAllNotExpired();

    JobGeneric findByGroupAndName(@Param("group") String group, @Param("name") String name);
}
