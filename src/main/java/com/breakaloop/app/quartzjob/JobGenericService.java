package com.breakaloop.app.quartzjob;

import com.breakaloop.app.quartzjob.entity.JobGeneric;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface JobGenericService {

    JobGeneric createJobAt(String group, String name, Map<String, Object> data, LocalDateTime localFireTime, ZoneId zoneId);

    JobGeneric createCronJob(String group, String name, Map<String, Object> data, String cronExp);

    JobGeneric getJob(String group, String name);

    void deleteJob(byte[] id);

    Optional<JobDescriptor> findJob(String group, String name);

    void updateJob(String group, String name, Map<String, Object> data);

    void deleteJob(String group, String name);

    void pauseJob(String group, String name);

    void resumeJob(String group, String name);
    
    List<JobGeneric> findAll();

    void initGenericJobs();
}
