package com.breakaloop.app.quartzjob.entity;

import com.breakaloop.constant.Constants;
import com.breakaloop.helper.BaseEntity;
import com.google.gson.Gson;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Getter
@Setter
@Entity
@Table(name = "job_generic")
public class JobGeneric extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 3574184005110730512L;

    @Column(name = "name")
    private String name;

    @Column(name = "[group]")
    private String group;

    @Column(name = "data")
    private String data;

    @Column(name = "active")
    private Boolean active = true;

    @Column(name = "cron_expression")
    private String cronExpression; // cron = null -> timer (has fireTime)

    @Column(name = "fire_time")
    private String fireTime;

    @Column(name = "expired_date", nullable = true, updatable = false)
    private Date expiredDate;

    @Transient
    private Map<String, Object> dataMap;
    @Transient
    private static Gson gson = new Gson();
    @Transient
    private LocalDateTime localFireTime;
    @Transient
    private ZoneId zoneId;

    public void setLocalFireTime(LocalDateTime localFireTime, ZoneId zoneId) throws ParseException {
        if (localFireTime != null && zoneId != null) {
            this.localFireTime = localFireTime;
            this.fireTime = getStringFromFireTime(localFireTime, zoneId);

            Date fireDate = getFireTimeFromString(this.fireTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fireDate);
            calendar.add(Calendar.DATE, 30);
            this.expiredDate = calendar.getTime();
        }
    }

    public static String getStringFromFireTime(LocalDateTime localFireTime, ZoneId zoneId) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.FULL_DATE_TIME_FORMAT);
        return localFireTime.atZone(zoneId).format(formatter);
    }

    public static Date getFireTimeFromString(String fireTimeStr) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.FULL_DATE_TIME_FORMAT);
        Date date = sdf.parse(fireTimeStr);
        return date;
    }

    public void setData(String data) {
        this.data = data;
        setDataMap(getDataMap(data));
    }

    public Map<String, Object> getDataMap() {
        return getDataMap(data);
    }

    public static Map<String, Object> getDataMap(String data) {
        Map dataMap = new HashMap<>();
        if (data != null && gson != null) {
            dataMap = gson.fromJson(data, Map.class);
        }
        return dataMap;
    }

    public void setDataMap(Map<String, Object> dataMap) {
        this.dataMap = dataMap;
        if (dataMap != null && gson != null) {
            this.data = gson.toJson(dataMap);
        }
    }
}
