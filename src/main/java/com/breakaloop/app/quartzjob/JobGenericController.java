package com.breakaloop.app.quartzjob;

import com.breakaloop.app.quartzjob.entity.JobGeneric;
import com.breakaloop.helper.UUIDUtils;
import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static com.breakaloop.constant.Constants.FULL_DATE_TIME_FORMAT;

@RequestMapping("/job")
@RestController
public class JobGenericController {
    @Inject
    JobGenericService jobGenericService;

    @Inject
    JobGenericRepository jobGenericRepository;

    @PostMapping("/test")
    public ResponseEntity<String> login(@RequestParam String group, @RequestParam String name, @RequestParam String fireTimeStr) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FULL_DATE_TIME_FORMAT);
        Date fireTime = null;
        try {
            fireTime = simpleDateFormat.parse(fireTimeStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ZoneId zoneId = ZoneId.systemDefault();
        LocalDateTime localDateTime = fireTime.toInstant().atZone(zoneId).toLocalDateTime();

        Map<String, Object> data = new HashMap<>();
        jobGenericService.createJobAt(group, name, data, localDateTime, zoneId);

        return ResponseEntity.status(HttpStatus.OK).body("");
    }

    @GetMapping("/test2")
    public JobGeneric test2(@RequestParam(required = false) String uuidStr, @RequestParam(required = false) String bytesStr) {
        List<JobGeneric> all = jobGenericRepository.findAll();
        for (JobGeneric jobGeneric : all) {
            System.out.println(jobGeneric.getId() + " : " + jobGeneric.getId() + " : " + jobGeneric.getId().toString() + " : " + jobGeneric.getName());
        }

        if (org.apache.commons.lang3.StringUtils.isNotBlank(uuidStr)) {
            UUID uuid = UUID.fromString(uuidStr);
            Optional<JobGeneric> optionalJobGeneric = jobGenericRepository.findById(UUIDUtils.asBytes(uuid));
            return optionalJobGeneric.get();
        } else {
            //String base64String = Base64.encodeBase64String(bytes);
            String base64String = bytesStr;
            byte[] backToBytes = Base64.decodeBase64(base64String);//0x2F0977D09B94431BA65759A5F38CAEC4
            System.out.println("BigInteger:"+Base64.decodeInteger(backToBytes));
            System.out.println("hex:"+UUIDUtils.bytesToHex(backToBytes)); //SELECT *,cast(id as varbinary(max)) from job_generic
            System.out.println(backToBytes);
            Optional<JobGeneric> optionalJobGeneric = jobGenericRepository.findById(backToBytes);
            return optionalJobGeneric.get();
        }
    }
}