package com.breakaloop.app.junitgenerator.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MyClass {
    Set<MyMethod> myMethods = new TreeSet<>();
}
