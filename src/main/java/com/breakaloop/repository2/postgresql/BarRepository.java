package com.breakaloop.repository2.postgresql;

import com.breakaloop.entity2.postgresql.Bar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BarRepository extends JpaRepository<Bar, Long> {

    Optional<Bar> findById(Long id);

}