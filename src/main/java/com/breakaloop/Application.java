package com.breakaloop;

import com.breakaloop.app.quartzjob.JobGenericService;
import com.breakaloop.app.sanball7.service.impl.ScheduleServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.SessionCookieConfig;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.TimeZone;

import static com.breakaloop.app.quartzjob.Constants.JOB_PACKAGE_ENTITY;
import static com.breakaloop.app.quartzjob.Constants.JOB_PACKAGE_REPOSITORY;
import static com.breakaloop.app.quartzjob.Constants.JOB_PACKAGE_SERVICE;
import static com.breakaloop.app.sanball7.constant.Constant.*;
import static com.breakaloop.scheduler.Constant.SCHEDULER_PACKAGE_CONFIG;
import static com.breakaloop.scheduler.Constant.SCHEDULER_PACKAGE_SERVICE;

@Controller
@EnableWebSecurity
//@SpringBootApplication//(scanBasePackageClasses = {otherpackage.MyClass.class, ...})
@EntityScan(basePackages = {
        "com.breakaloop.entity",
        SAN_BALL_7_PACKAGE_ENTITY,
        JOB_PACKAGE_ENTITY})
@EnableJpaRepositories(basePackages = {
        "com.breakaloop.repository",
        SAN_BALL_7_PACKAGE_REPOSITORY,
        JOB_PACKAGE_REPOSITORY})
//@ComponentScan(basePackages = {"com.breakaloop.angularbootstrap","com.example.balspringheroku"})
//@SpringBootApplication
@SpringBootApplication(scanBasePackages = {
        "com.breakaloop.interceptor",
        "com.breakaloop.service",
        "com.breakaloop.controller",
        "com.breakaloop.config",
        SAN_BALL_7_PACKAGE_SERVICE,
        JOB_PACKAGE_SERVICE,
        SCHEDULER_PACKAGE_SERVICE,
        SCHEDULER_PACKAGE_CONFIG})
@EnableJpaAuditing
//@EnableWebSecurity
//public class Application { // no facebook login
public class Application {//extends SpringBootServletInitializer { // cha khac gi
    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    public static void load(String link) {

        try {

            URL url = new URL(link);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }

            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }

    }

    @Scheduled(cron = "0/1 0 0/2 ? * * *") // every 2 hours
    public void scheduleTaskUsingCronExpression() throws IOException {
        long now = System.currentTimeMillis() / 1000;
        load("https://www.breakaloop.com/resources/favicon.ico");
        load("https://breakaloop.herokuapp.com/resources/favicon.ico");
        System.out.println("Schedule tasks using cron jobs - " + now);
    }

    @Bean
    public CommonsRequestLoggingFilter requestLoggingFilter() {
        CommonsRequestLoggingFilter loggingFilter = new CommonsRequestLoggingFilter();
        loggingFilter.setIncludeClientInfo(true);
        loggingFilter.setIncludeQueryString(true);
        loggingFilter.setIncludePayload(true);
        return loggingFilter;
    }

    @Bean
    public ServletContextInitializer servletContextInitializer() {
        return new ServletContextInitializer() {
            @Override
            public void onStartup(ServletContext servletContext) throws ServletException {
                SessionCookieConfig sessionCookieConfig = servletContext.getSessionCookieConfig();
                sessionCookieConfig.setName("JSESSIONID");
                sessionCookieConfig.setMaxAge(60*60*24*90); // 90 days
            }
        };
    }

    @PostConstruct
    void started() {
        log.info("Default Timezone: {}", TimeZone.getDefault());
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        log.info("Set Default Timezone: {}", TimeZone.getDefault());
    }

    @Autowired
    ScheduleServiceImpl scheduleServiceImpl;

    @Autowired
    JobGenericService jobGenericService;

    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
        log.info("On Start Up");
        scheduleServiceImpl.initOnStartUp();
        jobGenericService.initGenericJobs();
    }
}
