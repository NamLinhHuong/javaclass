//package com.breakaloop.scheduler.service;
//
//import org.quartz.*;
//import org.quartz.Trigger.TriggerState;
//import org.quartz.impl.matchers.GroupMatcher;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.annotation.Lazy;
//import org.springframework.scheduling.quartz.QuartzJobBean;
//import org.springframework.scheduling.quartz.SchedulerFactoryBean;
//import org.springframework.stereotype.Service;
//
//import java.util.*;
//
//@Service // not used here, used with extend
//public class JobServiceImpl implements com.breakaloop.scheduler.service.JobService {
//    private static final Logger log = LoggerFactory.getLogger(JobServiceImpl.class);
//
//    @Autowired
//    @Lazy
//    SchedulerFactoryBean schedulerFactoryBean;
//
//    @Autowired
//    private ApplicationContext context;
//
//    /**
//     * Schedule a job by jobName at given date.
//     */
//    @Override
//    public boolean scheduleOneTimeJob(String groupKey, String jobKey,
//                                      Class<? extends QuartzJobBean> jobClass,
//                                      Map<String, String> data, Date date) {
//        log.info("Job - schedule one time job: {} {}", groupKey, jobKey);
//
//        String triggerKey = jobKey;
//
//        JobDetail jobDetail = JobUtil.createJob(jobClass, false, context, jobKey, groupKey, data);
//
//        System.out.println("creating trigger for key :"+jobKey + " at date :"+date);
//        Trigger cronTriggerBean = JobUtil.createSingleTrigger(triggerKey, date, SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
//        //Trigger cronTriggerBean = JobUtil.createSingleTrigger(triggerKey, date, SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT);
//
//        try {
//            Scheduler scheduler = schedulerFactoryBean.getScheduler();
//            Date dt = scheduler.scheduleJob(jobDetail, cronTriggerBean);
//            System.out.println("Job with key jobKey :"+jobKey+ " and group :"+groupKey+ " scheduled successfully for date :"+dt);
//            return true;
//        } catch (SchedulerException e) {
//            log.error("SchedulerException while scheduling job {}-{}", groupKey, jobKey, e);
//        }
//
//        return false;
//    }
//
//    /**
//     * Schedule a job by jobName at given date.
//     */
//    @Override
//    public boolean scheduleCronJob(String groupKey, String jobKey,
//                                   Class<? extends QuartzJobBean> jobClass,
//                                   Map<String, String> data, Date date, String cronExpression) {
//        log.info("Job - schedule cron job: {} {}", groupKey, jobKey);
//
//        String triggerKey = jobKey;
//
//        JobDetail jobDetail = JobUtil.createJob(jobClass, false, context, jobKey, groupKey, data);
//
//        System.out.println("creating trigger for key :"+jobKey + " at date :"+date);
//        Trigger cronTriggerBean = JobUtil.createCronTrigger(triggerKey, date, cronExpression, SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
//
//        try {
//            Scheduler scheduler = schedulerFactoryBean.getScheduler();
//            Date dt = scheduler.scheduleJob(jobDetail, cronTriggerBean);
//            System.out.println("Job with key jobKey :"+jobKey+ " and group :"+groupKey+ " scheduled successfully for date :"+dt);
//            return true;
//        } catch (SchedulerException e) {
//            log.error("SchedulerException while scheduling job {}-{}", groupKey, jobKey, e);
//        }
//
//        return false;
//    }
//
//    /**
//     * Update one time scheduled job.
//     */
//    @Override
//    public boolean updateOneTimeJob(String groupKey, String jobKey, Date date) {
//        log.info("Job - one time job: {} {} {}", groupKey, jobKey, date);
//
//        try {
//            //Trigger newTrigger = JobUtil.createSingleTrigger(jobKey, date, SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT);
//            Trigger newTrigger = JobUtil.createSingleTrigger(jobKey, date, SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
//
//            Date dt = schedulerFactoryBean.getScheduler().rescheduleJob(TriggerKey.triggerKey(jobKey), newTrigger);
//            System.out.println("Trigger associated with jobKey :"+jobKey+ " rescheduled successfully for date :"+dt);
//            return true;
//        } catch ( Exception e ) {
//            log.error("SchedulerException while updating one time job {}-{}", groupKey, jobKey, e);
//            return false;
//        }
//    }
//
//    /**
//     * Update scheduled cron job.
//     */
//    @Override
//    public boolean updateCronJob(String groupKey, String jobKey, Date date, String cronExpression) {
//        log.info("Job - update cron job: {} {}", groupKey, jobKey);
//
//        try {
//            //Trigger newTrigger = JobUtil.createSingleTrigger(jobKey, date, SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT);
//            Trigger newTrigger = JobUtil.createCronTrigger(jobKey, date, cronExpression, SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
//
//            Date dt = schedulerFactoryBean.getScheduler().rescheduleJob(TriggerKey.triggerKey(jobKey), newTrigger);
//            System.out.println("Trigger associated with jobKey :"+jobKey+ " rescheduled successfully for date :"+dt);
//            return true;
//        } catch ( Exception e ) {
//            log.error("SchedulerException while updating job {}-{}", groupKey, jobKey, e);
//            return false;
//        }
//    }
//
//    /**
//     * Remove the indicated Trigger from the scheduler.
//     * If the related job does not have any other triggers, and the job is not durable, then the job will also be deleted.
//     */
//    @Override
//    public boolean unScheduleJob(String groupKey, String jobKey) {
//        log.info("Job - un-schedule job: {} {}", groupKey, jobKey);
//        TriggerKey tkey = new TriggerKey(jobKey);
//        try {
//            boolean status = schedulerFactoryBean.getScheduler().unscheduleJob(tkey);
//            System.out.println("Trigger associated with jobKey :"+jobKey+ " unscheduled with status :"+status);
//            return status;
//        } catch (SchedulerException e) {
//            log.error("SchedulerException while unscheduling job {}-{}", groupKey, jobKey, e);
//            return false;
//        }
//    }
//
//    /**
//     * Delete the identified Job from the ScheduleServiceImpl - and any associated Triggers.
//     */
//    @Override
//    public boolean deleteJob(String groupKey, String jobKey) {
//        log.info("Job - delete job: {} {}", groupKey, jobKey);
//        JobKey jkey = new JobKey(jobKey, groupKey);
//        try {
//            boolean status = schedulerFactoryBean.getScheduler().deleteJob(jkey);
//            System.out.println("Job with jobKey :"+jobKey+ " deleted with status :"+status);
//            return status;
//        } catch (SchedulerException e) {
//            log.error("SchedulerException while deleting job {}-{}", groupKey, jobKey, e);
//            return false;
//        }
//    }
//
//    /**
//     * Pause a job
//     */
//    @Override
//    public boolean pauseJob(String groupKey, String jobKey) {
//        log.info("Job - pause: {} {}", groupKey, jobKey);
//        JobKey jkey = new JobKey(jobKey, groupKey);
//        try {
//            schedulerFactoryBean.getScheduler().pauseJob(jkey);
//            System.out.println("Job with jobKey :"+jobKey+ " paused succesfully.");
//            return true;
//        } catch (SchedulerException e) {
//            log.error("SchedulerException while pausing job {}-{}", groupKey, jobKey, e);
//            return false;
//        }
//    }
//
//    /**
//     * Resume paused job
//     */
//    @Override
//    public boolean resumeJob(String groupKey, String jobKey) {
//        log.info("Job - resume: {} {}", groupKey, jobKey);
//        JobKey jKey = new JobKey(jobKey, groupKey);
//        try {
//            schedulerFactoryBean.getScheduler().resumeJob(jKey);
//            System.out.println("Job with jobKey :"+jobKey+ " resumed succesfully.");
//            return true;
//        } catch (SchedulerException e) {
//            log.error("SchedulerException while resuming job {}-{}", groupKey, jobKey, e);
//            return false;
//        }
//    }
//
//    @Override
//    public boolean startJobNow(String groupKey, String jobKey) {
//        log.info("Job - start: {} {}", groupKey, jobKey);
//        JobKey jKey = new JobKey(jobKey, groupKey);
//        try {
//            schedulerFactoryBean.getScheduler().triggerJob(jKey);
//            log.info("Job started successfully: {} {}", groupKey, jobKey);
//            return true;
//        } catch (SchedulerException e) {
//            log.error("SchedulerException while starting job {}-{}", groupKey, jobKey, e);
//            return false;
//        }
//    }
//
//    @Override
//    public boolean isJobRunning(String groupKey, String jobKey) {
//        log.info("Job - running: {} {}", groupKey, jobKey);
//        try {
//            List<JobExecutionContext> currentJobs = schedulerFactoryBean.getScheduler().getCurrentlyExecutingJobs();
//            if(currentJobs!=null){
//                for (JobExecutionContext jobCtx : currentJobs) {
//                    String jobNameDB = jobCtx.getJobDetail().getKey().getName();
//                    String groupNameDB = jobCtx.getJobDetail().getKey().getGroup();
//                    if (jobKey.equalsIgnoreCase(jobNameDB) && groupKey.equalsIgnoreCase(groupNameDB)) {
//                        return true;
//                    }
//                }
//            }
//        } catch (SchedulerException e) {
//            log.error("SchedulerException while checking job {}-{} is running", groupKey, jobKey, e);
//            return false;
//        }
//        return false;
//    }
//
//    /**
//     * Get all jobs
//     */
//    @Override
//    public List<Map<String, Object>> getAllJobs() {
//        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
//        try {
//            Scheduler scheduler = schedulerFactoryBean.getScheduler();
//
//            for (String groupName : scheduler.getJobGroupNames()) {
//                for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(groupName))) {
//
//                    String jobName = jobKey.getName();
//                    String jobGroup = jobKey.getGroup();
//
//                    //get job's trigger
//                    List<Trigger> triggers = (List<Trigger>) scheduler.getTriggersOfJob(jobKey);
//                    Date scheduleTime = triggers.get(0).getStartTime();
//                    Date nextFireTime = triggers.get(0).getNextFireTime();
//                    Date lastFiredTime = triggers.get(0).getPreviousFireTime();
//
//                    Map<String, Object> map = new HashMap<String, Object>();
//                    map.put("jobName", jobName);
//                    map.put("groupName", jobGroup);
//                    map.put("scheduleTime", scheduleTime);
//                    map.put("lastFiredTime", lastFiredTime);
//                    map.put("nextFireTime", nextFireTime);
//
//                    if(isJobRunning(jobGroup, jobName)){
//                        map.put("jobStatus", "RUNNING");
//                    }else{
//                        String jobState = getJobState(jobGroup, jobName);
//                        map.put("jobStatus", jobState);
//                    }
//
//                    /*                    Date currentDate = new Date();
//                    if (scheduleTime.compareTo(currentDate) > 0) {
//                        map.put("jobStatus", "scheduled");
//
//                    } else if (scheduleTime.compareTo(currentDate) < 0) {
//                        map.put("jobStatus", "Running");
//
//                    } else if (scheduleTime.compareTo(currentDate) == 0) {
//                        map.put("jobStatus", "Running");
//                    }*/
//
//                    list.add(map);
//                    System.out.println("Job details:");
//                    System.out.println("Job Name:"+jobName + ", Group Name:"+ groupName + ", Schedule Time:"+scheduleTime);
//                }
//
//            }
//        } catch (SchedulerException e) {
//            log.error("SchedulerException while fetching all jobs", e);
//        }
//        return list;
//    }
//
//    /**
//     * Check job exist with given name
//     */
//    @Override
//    public boolean isJobWithNamePresent(String groupKey, String jobKeyStr) {
//        try {
//            JobKey jobKey = new JobKey(jobKeyStr, groupKey);
//            Scheduler scheduler = schedulerFactoryBean.getScheduler();
//            if (scheduler.checkExists(jobKey)){
//                return true;
//            }
//        } catch (SchedulerException e) {
//            log.error("SchedulerException while checking job {}-{} exist", groupKey, jobKeyStr, e);
//        }
//        return false;
//    }
//
//    /**
//     * Get the current state of job
//     */
//    public String getJobState(String groupKey, String jobKeyStr) {
//        try {
//            JobKey jobKey = new JobKey(jobKeyStr, groupKey);
//
//            Scheduler scheduler = schedulerFactoryBean.getScheduler();
//            JobDetail jobDetail = scheduler.getJobDetail(jobKey);
//
//            List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobDetail.getKey());
//            if(triggers != null && triggers.size() > 0){
//                for (Trigger trigger : triggers) {
//                    TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
//
//                    if (TriggerState.PAUSED.equals(triggerState)) {
//                        return "PAUSED";
//                    }else if (TriggerState.BLOCKED.equals(triggerState)) {
//                        return "BLOCKED";
//                    }else if (TriggerState.COMPLETE.equals(triggerState)) {
//                        return "COMPLETE";
//                    }else if (TriggerState.ERROR.equals(triggerState)) {
//                        return "ERROR";
//                    }else if (TriggerState.NONE.equals(triggerState)) {
//                        return "NONE";
//                    }else if (TriggerState.NORMAL.equals(triggerState)) {
//                        return "SCHEDULED";
//                    }
//                }
//            }
//        } catch (SchedulerException e) {
//            log.error("SchedulerException while checking job {}-{} exist", groupKey, jobKeyStr, e);
//        }
//        return null;
//    }
//
//    /**
//     * Stop a job
//     */
//    @Override
//    public boolean stopJob(String groupKey, String jobKey) {
//        log.info("Job - stop: {} {}", groupKey, jobKey);
//        try{
//            Scheduler scheduler = schedulerFactoryBean.getScheduler();
//            JobKey jkey = new JobKey(jobKey, groupKey);
//
//            return scheduler.interrupt(jkey);
//
//        } catch (SchedulerException e) {
//            log.error("SchedulerException while stopping job {}-{}", groupKey, jobKey, e);
//        }
//        return false;
//    }
//}
//
