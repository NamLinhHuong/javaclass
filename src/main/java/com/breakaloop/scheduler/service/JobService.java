//package com.breakaloop.scheduler.service;
//
//import org.springframework.scheduling.quartz.QuartzJobBean;
//
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
//public interface JobService {
//	boolean scheduleOneTimeJob(String groupKey, String jobKey,
//							   Class<? extends QuartzJobBean> jobClass,
//							   Map<String, String> data, Date date);
//	boolean scheduleCronJob(String groupKey, String jobKey,
//							Class<? extends QuartzJobBean> jobClass,
//							Map<String, String> data, Date date, String cronExpression);
//
//	boolean updateOneTimeJob(String groupKey, String jobKey, Date date);
//	boolean updateCronJob(String groupKey, String jobKey, Date date, String cronExpression);
//
//	boolean unScheduleJob(String groupKey, String jobKey);
//	boolean deleteJob(String groupKey, String jobKey);
//	boolean pauseJob(String groupKey, String jobKey);
//	boolean resumeJob(String groupKey, String jobKey);
//	boolean startJobNow(String groupKey, String jobKey);
//	boolean isJobRunning(String groupKey, String jobKey);
//	List<Map<String, Object>> getAllJobs();
//	boolean isJobWithNamePresent(String groupKey, String jobKey);
//	String getJobState(String groupKey, String jobKey);
//	boolean stopJob(String groupKey, String jobKey);
//}
