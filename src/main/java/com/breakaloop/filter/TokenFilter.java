package com.breakaloop.filter;

import com.breakaloop.entity.User;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.*;
import java.io.IOException;

public class TokenFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {

        Object userId = request.getAttribute(AuthFilter.USER_ID);

        // save auth
        // some request don't have userId (not pass header or cookie, such as only get resource)
        if (userId != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            User user = new User();
            user.setUserId((String) userId);
            SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user, null, null));
        }

        chain.doFilter(request, response);
        System.out.println("OKKKKKE NAY:"+request.getParameter("token"));
    }

    @Override
    public void destroy() { /* unused */ }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException { /* unused */ }
}
