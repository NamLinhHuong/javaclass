package com.breakaloop.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

// block 1 day (TIME_RANGE_ONE_DAY) for ip access too much (> MAX_ACCESS per TIME_RANGE)
public class LimitAccessFilter implements Filter {

    private static final String
            LIMIT_ACCESS_FILTER = "LAF_",
            ACCESS_DENIED = null; // null -> save bandwidth ^.^
    private static final long TIME_RANGE = 10 * 60 * 1000; // 10 minutes
    private static final long MAX_ACCESS = 600 * TIME_RANGE; // 10 minutes
    private static final long TIME_RANGE_ONE_DAY = 60 * 60 * 1000 * 24;
    private static final int MAX_IP_SAVED = 1000;

    int blackListSize = 0;
    Set<String> blackList = new HashSet<>();
    int accessCountMapSize = 0;
    Map<String, Integer> accessCountMap = new HashMap<>();

    long initTimestamp = System.currentTimeMillis();
    long initTimestampEveryday = System.currentTimeMillis();

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        long time = System.currentTimeMillis();

        String requesterIp = request.getRemoteAddr();

        if (blackList.contains(requesterIp)) {
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            httpResponse.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, ACCESS_DENIED);
            return;
        }

        Integer accessCounter = accessCountMap.get(requesterIp);
        if (accessCounter == null) {
            accessCounter = 1;
        } else {
            accessCounter++;
        }

        if (accessCountMapSize > MAX_IP_SAVED) {
            accessCountMap.clear();
            accessCountMapSize = 0;
        }
        accessCountMap.put(requesterIp, accessCounter);
        accessCountMapSize++;

        // count access every 10 minutes
        long currentTimestamp = System.currentTimeMillis();
        if (currentTimestamp - initTimestamp > TIME_RANGE) {
            System.out.println(LIMIT_ACCESS_FILTER + requesterIp + "_" + accessCounter);

            // block
            if (accessCounter > MAX_ACCESS) {
                System.out.println(LIMIT_ACCESS_FILTER + "block" + "_" + requesterIp);
                if (blackListSize > MAX_IP_SAVED) {
                    blackList.clear();
                    blackListSize = 0;
                }
                blackList.add(requesterIp);
                blackListSize++;
            }

            initTimestamp = currentTimestamp;
        }
        if (currentTimestamp - initTimestampEveryday > TIME_RANGE_ONE_DAY) {
            System.out.println(LIMIT_ACCESS_FILTER + "reset_black_list");
            blackList.clear();
            initTimestampEveryday = currentTimestamp;
        }

        // continue
        chain.doFilter(request, response);

        // TODO: check time biggest -> monitor
        time = System.currentTimeMillis() - time;
        System.out.println(((HttpServletRequest) request).getRequestURI() + ": " + time + " ms");
    }

    @Override
    public void destroy() { /* unused */ }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException { /* unused */ }

}
