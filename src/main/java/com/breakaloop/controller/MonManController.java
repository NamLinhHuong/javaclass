//package com.breakaloop.controller;
//
//import com.breakaloop.entity.MonMan;
//import com.breakaloop.repository.MonManRepository;
//import com.breakaloop.service.MonManService;
//import com.breakaloop.service.impl.AuditorAwareImpl;
//import com.google.gson.Gson;
//import io.swagger.annotations.ApiParam;
//import lombok.extern.slf4j.Slf4j;
//import net.minidev.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.domain.Sort;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.social.ResourceNotFoundException;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.*;
//
//import java.math.BigDecimal;
//import java.util.List;
//import java.util.Optional;
//
//import static com.breakaloop.filter.AuthFilter.USER_ID;
//
//@RestController // = @Controller + @ResponseBody
//@RequestMapping("/api/mon-man/")
//@Slf4j
//public class MonManController {
//    @Autowired
//    MonManService monManService;
//
//    @Autowired
//    MonManRepository monManRepository;
//
//    @GetMapping(value = "/paging")
//    public ResponseEntity<Page> findPaginated(@RequestParam(name = "page", defaultValue = "0") Integer page,
//                                                      @RequestParam(name = "size", defaultValue = "10") Integer size) {
//        Pageable pageable = PageRequest.of(page, size, Sort.by(
//                //new Sort.Order(Sort.Direction.ASC, "lastName"),
//                new Sort.Order(Sort.Direction.DESC, "id")
//        ));
//
//        Page<MonMan> resultPage = monManService.findPaginated(pageable);
//        if (pageable.getPageNumber() > resultPage.getTotalPages()) {
//            pageable = PageRequest.of(resultPage.getTotalPages(), size, Sort.by(
//                    new Sort.Order(Sort.Direction.DESC, "id")
//            ));
//            resultPage = monManService.findPaginated(pageable);
//            //throw new ResourceNotFoundException("mon-man", "resources not found");
//        }
//        return new ResponseEntity<>(resultPage, HttpStatus.OK);
//    }
//
//    @GetMapping(value = "/list")
//    public ResponseEntity<List<MonMan>> list(@ApiParam(hidden = true) @RequestAttribute(name = USER_ID) String userId) {
//        List<MonMan> list = null;
//        if (userId != null) {
//            list = monManService.findByUserId(userId);
//            resetIds(list);
//            return new ResponseEntity<>(list, HttpStatus.OK);
//        } else {
//            return new ResponseEntity<>(list, HttpStatus.NOT_FOUND);
//        }
//    }
//
//    @DeleteMapping(value = "/{id}")
//    public ResponseEntity<MonMan> delete(
//            @ApiParam(hidden = true) @RequestAttribute(name = USER_ID) String userId,
//            @PathVariable("id") Long id) {
//        MonMan monMan = null;
//        if (userId != null) {
//            monMan = monManService.findById(id);
//            if (monMan != null) {
//                log.info("userId: " + userId + ", createdBy: " + monMan.getCreatedBy());
//                if (userId.equals(monMan.getCreatedBy())) {
//                    monManService.delete(monMan);
//                }
//            }
//            return new ResponseEntity<>(monMan, HttpStatus.OK);
//        } else {
//            return new ResponseEntity<>(monMan, HttpStatus.NOT_FOUND);
//        }
//    }
//
//    @GetMapping(value = "/{id}")
//    public ResponseEntity<MonMan> get(
//            @ApiParam(hidden = true) @RequestAttribute(name = USER_ID) String userId,
//            @PathVariable("id") Long id) {
//        MonMan monMan = null;
//        if (userId != null) {
//            monMan = monManService.findById(id);
//            if (monMan != null) {
//                return new ResponseEntity<>(monMan, HttpStatus.OK);
//            }
//        }
//        return new ResponseEntity<>(monMan, HttpStatus.NOT_FOUND);
//    }
//
//    @GetMapping(value = "/statistics")
//    public ResponseEntity<String> getStatistics(
//            @ApiParam(hidden = true) @RequestAttribute(name = USER_ID) String userId) {
//        List<MonMan> all = monManRepository.findAll();
//
//        BigDecimal totalIncome = new BigDecimal(0);
//        BigDecimal totalExpense = new BigDecimal(0);
//
//        for (MonMan monMan : all) {
//            if (MonMan.MonManType.EXPENSE.equals(monMan.getType())) {
//                totalExpense = totalExpense.add(monMan.getAmount());
//            } else if (MonMan.MonManType.INCOME.equals(monMan.getType())) {
//                totalIncome = totalIncome.add(monMan.getAmount());
//            }
//        }
//
//        JSONObject json = new JSONObject();
//        json.put("total", all.size());
//        json.put("totalIncome", totalIncome);
//        json.put("totalExpense", totalExpense);
//        return new ResponseEntity<>(json.toString(), HttpStatus.OK);
//    }
//
//    @PostMapping(value = "/add")
//    public MonMan add(@RequestBody(required = false) MonMan monMan,
//                      @ApiParam(hidden = true) @RequestAttribute(name = USER_ID) String userId) {
//        if (monMan == null) {
//            monMan = new MonMan();
//        }
//
//        Gson gson = new Gson();
//        String json = gson.toJson(monMan);
//        log.info("First");
//        log.info(json);
//        MonMan mm = gson.fromJson(json, MonMan.class);
//        log.info(mm.toString());
//
//        if (userId != null && monMan.getCreatedBy() == null) {
//            monMan.setCreatedBy(userId); // manually cause in case update createdBy not updated
//        }
//
//        MonMan result = monManService.save(monMan);
//
//        json = gson.toJson(result); // transient
//        log.info("Second");
//        log.info(json);
//        mm = gson.fromJson(json, MonMan.class);
//        log.info(mm.toString());
//
////        Optional<String> currentUserId = AuditorAwareImpl.getCurrentUserId();
////        List<MonMan> monManList = monManService.findByUserId(currentUserId.get());
////        resetIds(monManList);
//        return result;
//    }
//
//    private void resetIds(List<MonMan> list) {
////        if (list == null) return;
////        Integer i = 0;
////        for (MonMan item : list) {
////            item.setId((long) i);
////            i++;
////        }
//    }
//}
