package com.breakaloop.controller;


import com.breakaloop.entity.JwtUser;
import com.breakaloop.helper.AuthUtils;
import com.breakaloop.helper.PasswordService;
import com.breakaloop.helper.Token;
import com.breakaloop.service.JwtUserService;
import com.nimbusds.jose.JOSEException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import static com.breakaloop.constant.Constants.LOGING_ERROR_MSG;

//import org.apache.log4j.Logger;

/**
 * JWT Authentcation controller
 * @author Carlos
 */
@RestController
public class LocalAuthenticationController {
 //   private static Logger LOG = Logger.getLogger(LocalAuthenticationController.class);
 
    @Autowired
    private JwtUserService jwtUserService;

    @PostMapping("/auth/login")
    public ResponseEntity login(@RequestBody @Valid final JwtUser jwtUser, @Context final HttpServletRequest request)
            throws JOSEException {
//        LOG.info("JwtUser: '" + jwtUser + "'");
        final JwtUser foundJwtUser = jwtUserService.findByEmail(jwtUser.getEmail());
        if (foundJwtUser != null
                && PasswordService.checkPassword(jwtUser.getPassword(), foundJwtUser.getPassword())) {
            final Token token = AuthUtils.createToken(request.getRemoteHost(), foundJwtUser.getId());
            return new ResponseEntity<Token>(token, HttpStatus.OK);
        }
        return new ResponseEntity<String>(LOGING_ERROR_MSG, HttpStatus.UNAUTHORIZED);
    }

    @PostMapping("/auth/signup")
    public ResponseEntity signup(@RequestBody @Valid final JwtUser jwtUser, @Context final HttpServletRequest request)
            throws JOSEException {
        jwtUser.setPassword(PasswordService.hashPassword(jwtUser.getPassword()));
        final JwtUser savedJwtUser = jwtUserService.save(jwtUser);
        final Token token = AuthUtils.createToken(request.getRemoteHost(), savedJwtUser.getId());
        return new ResponseEntity<Token>(token, HttpStatus.CREATED);
    }  
   
}
