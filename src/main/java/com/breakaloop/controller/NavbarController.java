//package com.breakaloop.controller;
//
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//
//@Controller
//public class NavbarController {
//
//    @GetMapping("/navbar-fixed-top")
//    public String navbarFixedTop(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
//        model.addAttribute("name", name);
//        model.addAttribute("abc", "fffffff");
//        return "navbar-fixed-top";
//    }
//
//    @GetMapping("/navbar-static-top")
//    public String navbarStaticTop(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
//        return "navbar-static-top";
//    }
//
//    @GetMapping("/navbar")
//    public String navbar(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
//        return "navbar";
//    }
//}