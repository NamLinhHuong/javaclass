package com.breakaloop.controller;

import com.breakaloop.entity.ApiHealthCheck;
import com.breakaloop.repository.ApiHealthCheckRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;


@RequestMapping("/api/")
@RestController
public class ApiController {
    @Inject
    ApiHealthCheckRepository apiHealthCheckRepository;

    @GetMapping("health-check")
    public ResponseEntity<String> healthCheck(HttpServletRequest httpReq) {
        ApiHealthCheck apiHealthCheck = new ApiHealthCheck();
        apiHealthCheck.setIp(httpReq.getRemoteAddr());
        apiHealthCheck.setTriggerDate(new Date());
        apiHealthCheckRepository.save(apiHealthCheck);
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }
}
