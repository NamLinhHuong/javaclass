//package com.breakaloop.controller;
//
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import com.breakaloop.scheduler.service.JobService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Lazy;
//import org.springframework.format.annotation.DateTimeFormat;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.breakaloop.scheduler.dto.ServerResponse;
//import com.breakaloop.scheduler.job.CronJob;
//import com.breakaloop.scheduler.job.SimpleJob;
//import com.breakaloop.scheduler.util.ServerResponseCode;
//
//import static com.breakaloop.constant.Constants.FULL_DATE_TIME;
//
//
//@RestController
//@RequestMapping("/scheduler/")
//public class JobController {
//
//    @Autowired
//    @Lazy
//    JobService jobService;
//
//    @GetMapping("schedule")
//    public ServerResponse schedule(
//            @RequestParam("groupKey") String groupKey,
//            @RequestParam("jobKey") String jobKey,
//            @RequestParam("jobScheduleTime") @DateTimeFormat(pattern = FULL_DATE_TIME) Date jobScheduleTime,
//            @RequestParam(value = "cronExpression", required = false) String cronExpression){
//        //Job Name is mandatory
//        if(groupKey == null || groupKey.trim().equals("")
//                || jobKey == null || jobKey.trim().equals("")){
//            return getServerResponse(ServerResponseCode.JOB_IDENTITY_NOT_PRESENT, false);
//        }
//
//        //Check if job Name is unique;
//        if(!jobService.isJobWithNamePresent(groupKey, jobKey)){
//
//            if(cronExpression == null || cronExpression.trim().equals("")){
//                //Single Trigger
//                boolean status = jobService.scheduleOneTimeJob(groupKey, jobKey, SimpleJob.class, new HashMap<>(), jobScheduleTime);
//                if(status){
//                    return getServerResponse(ServerResponseCode.SUCCESS, jobService.getAllJobs());
//                }else{
//                    return getServerResponse(ServerResponseCode.ERROR, false);
//                }
//
//            }else{
//                //Cron Trigger
//                boolean status = jobService.scheduleCronJob(groupKey, jobKey, CronJob.class, new HashMap<>(), jobScheduleTime, cronExpression);
//                if(status){
//                    return getServerResponse(ServerResponseCode.SUCCESS, jobService.getAllJobs());
//                }else{
//                    return getServerResponse(ServerResponseCode.ERROR, false);
//                }
//            }
//        }else{
//            return getServerResponse(ServerResponseCode.JOB_WITH_SAME_NAME_EXIST, false);
//        }
//    }
//
//    @GetMapping("unschedule")
//    public void unschedule(
//            @RequestParam("groupKey") String groupKey,
//            @RequestParam("jobKey") String jobKey) {
//        jobService.unScheduleJob(groupKey, jobKey);
//    }
//
//    @GetMapping("delete")
//    public ServerResponse delete(
//            @RequestParam("groupKey") String groupKey,
//            @RequestParam("jobKey") String jobKey) {
//        if(jobService.isJobWithNamePresent(groupKey, jobKey)){
//            boolean isJobRunning = jobService.isJobRunning(groupKey, jobKey);
//
//            if(!isJobRunning){
//                boolean status = jobService.deleteJob(groupKey, jobKey);
//                if(status){
//                    return getServerResponse(ServerResponseCode.SUCCESS, true);
//                }else{
//                    return getServerResponse(ServerResponseCode.ERROR, false);
//                }
//            }else{
//                return getServerResponse(ServerResponseCode.JOB_ALREADY_IN_RUNNING_STATE, false);
//            }
//        }else{
//            //Job doesn't exist
//            return getServerResponse(ServerResponseCode.JOB_DOESNT_EXIST, false);
//        }
//    }
//
//    @GetMapping("pause")
//    public ServerResponse pause(
//            @RequestParam("groupKey") String groupKey,
//            @RequestParam("jobKey") String jobKey) {
//        if(jobService.isJobWithNamePresent(groupKey, jobKey)){
//
//            boolean isJobRunning = jobService.isJobRunning(groupKey, jobKey);
//
//            if(!isJobRunning){
//                boolean status = jobService.pauseJob(groupKey, jobKey);
//                if(status){
//                    return getServerResponse(ServerResponseCode.SUCCESS, true);
//                }else{
//                    return getServerResponse(ServerResponseCode.ERROR, false);
//                }
//            }else{
//                return getServerResponse(ServerResponseCode.JOB_ALREADY_IN_RUNNING_STATE, false);
//            }
//
//        }else{
//            //Job doesn't exist
//            return getServerResponse(ServerResponseCode.JOB_DOESNT_EXIST, false);
//        }
//    }
//
//    @GetMapping("resume")
//    public ServerResponse resume(
//            @RequestParam("groupKey") String groupKey,
//            @RequestParam("jobKey") String jobKey) {
//        if(jobService.isJobWithNamePresent(groupKey, jobKey)){
//            String jobState = jobService.getJobState(groupKey, jobKey);
//
//            if(jobState.equals("PAUSED")){
//                System.out.println("Job current state is PAUSED, Resuming job...");
//                boolean status = jobService.resumeJob(groupKey, jobKey);
//
//                if(status){
//                    return getServerResponse(ServerResponseCode.SUCCESS, true);
//                }else{
//                    return getServerResponse(ServerResponseCode.ERROR, false);
//                }
//            }else{
//                return getServerResponse(ServerResponseCode.JOB_NOT_IN_PAUSED_STATE, false);
//            }
//
//        }else{
//            //Job doesn't exist
//            return getServerResponse(ServerResponseCode.JOB_DOESNT_EXIST, false);
//        }
//    }
//
//    @GetMapping("update")
//    public ServerResponse updateJob(
//            @RequestParam("groupKey") String groupKey,
//            @RequestParam("jobKey") String jobKey,
//            @RequestParam("jobScheduleTime") @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm") Date jobScheduleTime,
//            @RequestParam("cronExpression") String cronExpression){
//        System.out.println("JobController.updateJob()");
//
//        //Job Name is mandatory
//        if(groupKey == null || groupKey.trim().equals("")
//                || jobKey == null || jobKey.trim().equals("")){
//            return getServerResponse(ServerResponseCode.JOB_IDENTITY_NOT_PRESENT, false);
//        }
//
//        //Edit Job
//        if(jobService.isJobWithNamePresent(groupKey, jobKey)){
//
//            if(cronExpression == null || cronExpression.trim().equals("")){
//                //Single Trigger
//                boolean status = jobService.updateOneTimeJob(groupKey, jobKey, jobScheduleTime);
//                if(status){
//                    return getServerResponse(ServerResponseCode.SUCCESS, jobService.getAllJobs());
//                }else{
//                    return getServerResponse(ServerResponseCode.ERROR, false);
//                }
//
//            }else{
//                //Cron Trigger
//                boolean status = jobService.updateCronJob(groupKey, jobKey, jobScheduleTime, cronExpression);
//                if(status){
//                    return getServerResponse(ServerResponseCode.SUCCESS, jobService.getAllJobs());
//                }else{
//                    return getServerResponse(ServerResponseCode.ERROR, false);
//                }
//            }
//
//
//        }else{
//            return getServerResponse(ServerResponseCode.JOB_DOESNT_EXIST, false);
//        }
//    }
//
//    @GetMapping("jobs")
//    public ServerResponse getAllJobs(){
//        List<Map<String, Object>> list = jobService.getAllJobs();
//        return getServerResponse(ServerResponseCode.SUCCESS, list);
//    }
//
//    @GetMapping("checkJobName")
//    public ServerResponse checkJobName(
//            @RequestParam("groupKey") String groupKey,
//            @RequestParam("jobKey") String jobKey){
//        //Job Name is mandatory
//        if(groupKey == null || groupKey.trim().equals("")
//                || jobKey == null || jobKey.trim().equals("")){
//            return getServerResponse(ServerResponseCode.JOB_IDENTITY_NOT_PRESENT, false);
//        }
//
//        boolean status = jobService.isJobWithNamePresent(groupKey, jobKey);
//        return getServerResponse(ServerResponseCode.SUCCESS, status);
//    }
//
//    @GetMapping("isJobRunning")
//    public ServerResponse isJobRunning(
//            @RequestParam("groupKey") String groupKey,
//            @RequestParam("jobKey") String jobKey) {
//        boolean status = jobService.isJobRunning(groupKey, jobKey);
//        return getServerResponse(ServerResponseCode.SUCCESS, status);
//    }
//
//    @GetMapping("jobState")
//    public ServerResponse getJobState(
//            @RequestParam("groupKey") String groupKey,
//            @RequestParam("jobKey") String jobKey) {
//        String jobState = jobService.getJobState(groupKey, jobKey);
//        return getServerResponse(ServerResponseCode.SUCCESS, jobState);
//    }
//
//    @GetMapping("stop")
//    public ServerResponse stopJob(
//            @RequestParam("groupKey") String groupKey,
//            @RequestParam("jobKey") String jobKey) {
//        if(jobService.isJobWithNamePresent(groupKey, jobKey)){
//
//            if(jobService.isJobRunning(groupKey, jobKey)){
//                boolean status = jobService.stopJob(groupKey, jobKey);
//                if(status){
//                    return getServerResponse(ServerResponseCode.SUCCESS, true);
//                }else{
//                    //Server error
//                    return getServerResponse(ServerResponseCode.ERROR, false);
//                }
//
//            }else{
//                //Job not in running state
//                return getServerResponse(ServerResponseCode.JOB_NOT_IN_RUNNING_STATE, false);
//            }
//
//        }else{
//            //Job doesn't exist
//            return getServerResponse(ServerResponseCode.JOB_DOESNT_EXIST, false);
//        }
//    }
//
//    @GetMapping("start")
//    public ServerResponse startJobNow(
//            @RequestParam("groupKey") String groupKey,
//            @RequestParam("jobKey") String jobKey) {
//        if(jobService.isJobWithNamePresent(groupKey, jobKey)){
//
//            if(!jobService.isJobRunning(groupKey, jobKey)){
//                boolean status = jobService.startJobNow(groupKey, jobKey);
//
//                if(status){
//                    //Success
//                    return getServerResponse(ServerResponseCode.SUCCESS, true);
//
//                }else{
//                    //Server error
//                    return getServerResponse(ServerResponseCode.ERROR, false);
//                }
//
//            }else{
//                //Job already running
//                return getServerResponse(ServerResponseCode.JOB_ALREADY_IN_RUNNING_STATE, false);
//            }
//
//        }else{
//            //Job doesn't exist
//            return getServerResponse(ServerResponseCode.JOB_DOESNT_EXIST, false);
//        }
//    }
//
//    public ServerResponse getServerResponse(int responseCode, Object data){
//        ServerResponse serverResponse = new ServerResponse();
//        serverResponse.setStatusCode(responseCode);
//        serverResponse.setData(data);
//        return serverResponse;
//    }
//}
