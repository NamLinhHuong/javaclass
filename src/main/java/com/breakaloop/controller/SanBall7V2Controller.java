package com.breakaloop.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.breakaloop.app.sanball7.constant.ApiPath.API_V2;

@RequestMapping(API_V2)
@RestController
public class SanBall7V2Controller extends com.breakaloop.app.sanball7.controller.V2Controller {
}
