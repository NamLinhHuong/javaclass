package com.breakaloop.controller;


import com.breakaloop.entity.JwtUser;
import com.breakaloop.helper.AuthUtils;
import com.breakaloop.service.JwtUserService;
import com.nimbusds.jose.JOSEException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import java.text.ParseException;

/**
 *
 * @author Carlos
 */
@RestController
public class UserProfileController {

    @Autowired
    private JwtUserService jwtUserService;

    @GetMapping("/profile")
    public ResponseEntity findUser(@Context final HttpServletRequest request)
            throws JOSEException {
        JwtUser foundJwtUser = null;
        try {
            foundJwtUser = getAuthUser(request);
            if (foundJwtUser == null) {
                return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<JwtUser>(foundJwtUser, HttpStatus.CREATED);
    }

    private JwtUser getAuthUser(HttpServletRequest request) throws JOSEException, ParseException {
        String subject = AuthUtils.getSubject(request.getHeader(AuthUtils.AUTH_HEADER_KEY));
        return jwtUserService.findOne(subject);
    }
}
