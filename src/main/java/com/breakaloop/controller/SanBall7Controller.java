package com.breakaloop.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.breakaloop.app.sanball7.constant.ApiPath.API;

@RequestMapping(API)
@RestController
public class SanBall7Controller extends com.breakaloop.app.sanball7.controller.SanBall7Controller {
}
