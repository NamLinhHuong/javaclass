package com.breakaloop.controller;//package com.breakaloop.controller;
//
//import com.Foo;
//import com.Bar;
//import com.FooRepository;
//import com.BarRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.Optional;
//
//@RestController
//public class MultiSourceController {
//
//    private final FooRepository fooRepo;
//    private final BarRepository barRepo;
//
//    @Autowired
//    MultiSourceController(FooRepository fooRepo, BarRepository barRepo) {
//        this.fooRepo = fooRepo;
//        this.barRepo = barRepo;
//    }
//
//    @RequestMapping("/foobar/{id}")
//    public String fooBar(@PathVariable("id") Long id) {
//        Optional<Foo> foo = fooRepo.findById(id);
//        Optional<Bar> bar = barRepo.findById(id);
//
//        return foo.get().getFoo() + " " + bar.get().getBar();
//    }
//}