package com.breakaloop.controller;

import com.breakaloop.dto.Book;
import com.breakaloop.entity.UserConnection;
import com.breakaloop.repository.UserConnectionRepository;
import com.breakaloop.service.BookService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.breakaloop.filter.AuthFilter.USER_ID;

@Controller
public class HomeController {
    @GetMapping("/angularjs-bootstrap/home")
    @CrossOrigin(origins = "http://localhost:3000")
    public String angularjsBootstrap() {
        return "angularjs-bootstrap/index";
    }

    @Autowired
    BookService bookService;

    @ResponseBody
    @GetMapping(value="/redis-clear")
    public String redisClear() {
        bookService.deleteAll();
        Book.resetId();
        return "SUCCESS";
    }

    @ResponseBody
    @GetMapping(value="/redis-create")
    public String redisCreate() {
        // https://jira.spring.io/browse/DATAREDIS-175
        Book book = new Book(Book.generateNextId());
        bookService.save(book);
        List<Book> all = bookService.findAll();
        String test = "ok";
        for (Book b : all) {
            test += "_"+b.getId();
        }
        return test;
    }

    @ResponseBody
    @GetMapping(value="/api/test")
    public String apiTest() {
        return "SUCCESS";
    }

    @Autowired
    UserConnectionRepository userConnectionRepository;

    @ResponseBody
    @GetMapping(value="/api/profile")
    public ResponseEntity<UserConnection> getProfile(@ApiParam(hidden = true) @RequestAttribute(name=USER_ID) String userId) {
        if (userId == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        UserConnection userConnection = userConnectionRepository.findById(userId);
        return new ResponseEntity<>(userConnection, HttpStatus.OK);
    }
}
